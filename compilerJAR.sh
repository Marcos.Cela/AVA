#!/bin/bash
clear

time command jar -cvfm Juego.jar MANIFEST.MF * || {
    echo 'Could not compile sources, check for errors.' ;
    exit 1; 
}&&{
    echo ""
    echo ""
    echo 'Correctly compiled!'
    echo 'Setting permissions for jar file'
	chmod 755 PublicGoodsGame.jar
    }
