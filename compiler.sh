#!/bin/bash
clear
#Some variables
SOURCE_VER="1.6"
#Here is the path to the *.jars we are using in our project
LIB_PATH="jars/"
#Here is the path to the *.java we are using in our project
SRC_PATH="src/"
#Folder to save compiled *.class
OUTPUT_PATH="bin"
#Inform the user where the script will search for *.java files and for classpath
printf "Searching for *.java files on: \n\t" 
realpath "$SRC_PATH"
printf "Searching for *.jar files on: \n\t" 
realpath "$LIB_PATH"
printf "Output folder for *.class files will be:: \n\t" 
realpath "$OUTPUT_PATH"
printf "\n\n"


#Tell the user what is happening,to add some debug capacity
echo "Source version used is: $SOURCE_VER"
echo ""
echo "Detected sources in: $SRC_PATH"	 
for FILE in $SRC_PATH*.java; do
	printf "\t%s\n"  "$FILE"
done
# Tell the user what .jar files are being used
printf "\n\nLinking sources with  .jar files located at :%s\n" "$LIB_PATH"
for FILE in $LIB_PATH*.jar; do
	 printf "\t%s\n"  "$FILE"
done
printf "\nCompiling, total time:\n\t"

time command javac -source $SOURCE_VER -cp "$LIB_PATH*" -d $OUTPUT_PATH $SRC_PATH*.java || {
    echo 'Could not compile sources, check for errors.' ;
    exit 1; 
}&&{
    echo ""
    echo ""
    echo 'Correctly compiled, generated files can be found at: '
    realpath $OUTPUT_PATH
    echo ""
    }
