import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.MenuShortcut;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import org.jfree.chart.ChartPanel;

import jade.core.AID;
import jade.core.Agent;

/**
 * Visual implementation of the main menu of the program.
 *
 * @author markoscl
 */
@SuppressWarnings({ "serial" })
public class MainWindow extends JFrame {
    /** Formatter for stats */
    protected final static SimpleDateFormat timeFormatter = new SimpleDateFormat("mm:ss:SSS");
    private static MainWindow instance;
    /** This is the command to show money stats for each player */
    public final static String SHOW_MONEY_STATS = "Show money Stats";
    /** This is the command to show win stats for each player */
    public final static String SHOW_WIN_STATS = "Show win ratio stats";
    /** This is the commad to show role stats for each player */
    public final static String SHOW_ROLE_TATS = "Show role stats";
    /** Avaliale logging values */
    final static String[] logValues = { "ALL", "FINEST", "FINER", "FINE", "INFO", "CONFIG", "WARNING", "SEVERE",
	    "OFF" };

    /** The table that contains data about players */
    private static JTable playersInfoTable;
    /*
     * Fields for visual elements
     */
    private static JSpinner startingMoneySpinner;
    private static JSpinner potRatioSpinner;
    private static JSpinner fineFactorSpinner;
    private static JSpinner moneyToPutSpinner;
    private static JSpinner roundCounterSpinner;
    private static JSpinner inspectorsSpinner;
    private static JProgressBar gameProgressBar;

    /**
     * Fields for stats
     */
    private static MathUtils.SMA estimatedTimeAverage = new MathUtils.SMA(150);
    private static MathUtils.SMA roundSpeedAverag = new MathUtils.SMA(150);
    private static double lastUpdate = 0;

    /**
     * Message buffer for Logs
     */
    private static final ArrayList<String> logBuffer = new ArrayList<String>(250);
    /**
     * This is the main agent, we are saving it so we can call methods to
     * extract information, pause and start the game and so on.
     */
    private static psi7_MainAg mainAgent;
    // Buttons

    private JButton butonNew;
    private JButton buttonStop;
    private JButton buttonContinue;
    private JTable additionalInfoTable;
    private static JTextArea verbosePane;
    private long lastUpdateTime = System.currentTimeMillis();

    public MainWindow() {
	instance = this;// Singleton
	// Set main Window properties
	setTitle("Public Goods Game");
	getContentPane().setBackground(Color.LIGHT_GRAY);
	setDefaultCloseOperation(EXIT_ON_CLOSE);
	setExtendedState(Frame.MAXIMIZED_BOTH);
	setBackground(Color.LIGHT_GRAY);
	setBackground(Color.DARK_GRAY);
	// Preferred size to window working area
	setPreferredSize(new Dimension(-1, -1));
	// Maximize it when possible
	setSize(new Dimension(-1, -1));// Maximized
	// We dont set the Widhts or Heights, and let the elements contained in
	// this layout to automatically adjust the space they need
	GridBagLayout gridBagLayout = new GridBagLayout();
	gridBagLayout.rowWeights = new double[] { 1.0, 1.0 };
	gridBagLayout.columnWeights = new double[] { 0.0, 1.0 };
	getContentPane().setLayout(gridBagLayout);

	// Proceeds to draw the main window
	MenuBar menuBar = new MenuBar();
	// Fills menuBar with some items
	Menu editMenu = new Menu("Edit");
	MenuItem resetPlayerMenu = new MenuItem("Reset Players", new MenuShortcut('r'));
	resetPlayerMenu.addActionListener(new resetPlayerListener());
	editMenu.add(resetPlayerMenu);

	Menu helpMenu = new Menu("Help");
	MenuItem aboutMenu = new MenuItem("About");
	aboutMenu.addActionListener(new AboutListener());
	helpMenu.add(aboutMenu);
	menuBar.add(editMenu);
	menuBar.add(helpMenu);
	setMenuBar(menuBar);

	JPanel controlsPanel = new JPanel();
	controlsPanel.setBorder(new TitledBorder(new LineBorder(Color.GRAY, 5), "Controls"));
	GridBagConstraints gbc_controlsPanel = new GridBagConstraints();
	gbc_controlsPanel.insets = new Insets(0, 0, 5, 5);
	gbc_controlsPanel.anchor = GridBagConstraints.NORTHWEST;
	gbc_controlsPanel.gridx = 0;
	gbc_controlsPanel.gridy = 0;
	getContentPane().add(controlsPanel, gbc_controlsPanel);
	controlsPanel.setBackground(Color.LIGHT_GRAY);
	// We force the controls to be in vertical (Ones on top of others)
	controlsPanel.setLayout(new GridLayout(0, 1, 0, 0));
	// Buttons container, border layout to make it bigger
	JPanel buttonsContainer = new JPanel();
	buttonsContainer.setLayout(new BorderLayout(0, 0));
	// Buttons panel, they are forced to be in vertical
	JPanel buttonsPanel = new JPanel();
	buttonsContainer.add(buttonsPanel, BorderLayout.NORTH);
	buttonsPanel.setLayout(new GridLayout(0, 1, 0, 0));
	/**
	 * We add now the buttons. Each button has its own panel, in a
	 * BorderLayout style, so they use all the panel. This makes the buttons
	 * exactly the same size instead of different sized buttons.
	 */
	// Containers
	JPanel newbtnContainer = new JPanel();
	JPanel stopContainer = new JPanel();
	JPanel contContainer = new JPanel();
	butonNew = new JButton("New");
	butonNew.addActionListener(new NewGameListener());
	buttonStop = new JButton("Stop");
	buttonStop.addActionListener(new StopGameListener());

	buttonContinue = new JButton("Continue");
	buttonContinue.addActionListener(new ContinueGameListener());
	// Colors
	newbtnContainer.setBackground(Color.LIGHT_GRAY);
	stopContainer.setBackground(Color.LIGHT_GRAY);
	contContainer.setBackground(Color.LIGHT_GRAY);
	// Border layout so they use all the container space
	newbtnContainer.setLayout(new BorderLayout());
	stopContainer.setLayout(new BorderLayout());
	contContainer.setLayout(new BorderLayout());
	// Add buttons to button containers
	newbtnContainer.add(butonNew);
	stopContainer.add(buttonStop);
	contContainer.add(buttonContinue);
	// Add button containers to button panel
	buttonsPanel.add(newbtnContainer);
	buttonsPanel.add(stopContainer);
	buttonsPanel.add(contContainer);
	/**
	 * Add button panel on top left corner and set it to horizontally
	 */
	GridBagConstraints gbc_buttonsContainer = new GridBagConstraints();
	gbc_buttonsContainer.fill = GridBagConstraints.HORIZONTAL;
	gbc_buttonsContainer.gridx = 0;
	gbc_buttonsContainer.gridy = 0;
	controlsPanel.add(buttonsContainer, gbc_buttonsContainer);

	JPanel controlsContainer = new JPanel();
	buttonsContainer.add(controlsContainer, BorderLayout.SOUTH);
	controlsContainer.setAlignmentY(Component.TOP_ALIGNMENT);
	GridBagConstraints gbc_rolesInfo = new GridBagConstraints();
	gbc_rolesInfo.fill = GridBagConstraints.HORIZONTAL;
	gbc_rolesInfo.anchor = GridBagConstraints.CENTER;
	gbc_rolesInfo.gridx = 0;
	gbc_rolesInfo.gridy = 2;
	controlsContainer.setLayout(new GridLayout(0, 1, 0, 0));
	JTextPane startingMoneyTxt = new JTextPane();
	startingMoneyTxt.setText("Starting money:");
	startingMoneyTxt.setBackground(Color.LIGHT_GRAY);
	controlsContainer.add(startingMoneyTxt);

	startingMoneySpinner = new JSpinner();
	startingMoneySpinner.setModel(new SpinnerNumberModel(new Integer(5000), new Integer(0),
		new Integer(Integer.MAX_VALUE / 10000), new Integer(10000)));
	controlsContainer.add(startingMoneySpinner);

	JTextPane potRatioTxt = new JTextPane();
	potRatioTxt.setText("Interest ratio(n)");
	potRatioTxt.setBackground(Color.LIGHT_GRAY);
	controlsContainer.add(potRatioTxt);

	potRatioSpinner = new JSpinner();
	potRatioSpinner.setEnabled(true);
	potRatioSpinner.setModel(
		new SpinnerNumberModel(new Double(1.5F), new Double(1.05F), new Double(1.95F), new Double(0.05F)));

	controlsContainer.add(potRatioSpinner);

	JTextPane fineFactorTxt = new JTextPane();
	fineFactorTxt.setText("Fine factor (m)");
	fineFactorTxt.setBackground(Color.LIGHT_GRAY);
	controlsContainer.add(fineFactorTxt);

	fineFactorSpinner = new JSpinner();
	fineFactorSpinner.setModel(
		new SpinnerNumberModel(new Double(0.5F), new Double(0.05F), new Double(0.95F), new Double(0.05F)));

	controlsContainer.add(fineFactorSpinner);

	JTextPane moneyToPutTxt = new JTextPane();
	controlsContainer.add(moneyToPutTxt);
	moneyToPutTxt.setBackground(Color.LIGHT_GRAY);
	moneyToPutTxt.setText("Money to put (x)");

	moneyToPutSpinner = new JSpinner();
	controlsContainer.add(moneyToPutSpinner);
	moneyToPutSpinner.setModel(new SpinnerNumberModel(new Double(10000), new Double(0), null, new Double(1000)));

	JTextPane nRoundsText = new JTextPane();
	controlsContainer.add(nRoundsText);
	nRoundsText.setBackground(Color.LIGHT_GRAY);
	nRoundsText.setText("Number of rounds");

	roundCounterSpinner = new JSpinner();
	controlsContainer.add(roundCounterSpinner);
	roundCounterSpinner.setModel(new SpinnerNumberModel(new Integer(150), new Integer(0),
		new Integer(Integer.MAX_VALUE / 1000), new Integer(100)));
	roundCounterSpinner.setValue(1000);

	JTextPane nInspectorsTxt = new JTextPane();
	nInspectorsTxt.setText("Number of Inspectors");
	nInspectorsTxt.setBackground(Color.LIGHT_GRAY);
	controlsContainer.add(nInspectorsTxt);

	inspectorsSpinner = new JSpinner();
	inspectorsSpinner.setModel(new SpinnerNumberModel(new Integer(2), new Integer(0), null, new Integer(1)));
	controlsContainer.add(inspectorsSpinner);

	JButton verbose = new JButton("Verbose");
	controlsContainer.add(verbose);

	verbose.addActionListener(new ChangeLogLevelListener());

	JTabbedPane tabbedPane = new JTabbedPane();
	GridBagConstraints gbc_splitPane = new GridBagConstraints();
	gbc_splitPane.fill = GridBagConstraints.BOTH;
	gbc_splitPane.insets = new Insets(0, 0, 5, 0);
	gbc_splitPane.gridx = 1;
	gbc_splitPane.gridy = 0;

	JScrollPane tableContainer = new JScrollPane();
	tableContainer.setBackground(Color.LIGHT_GRAY);
	TableModel model = toTableModel(getPlayerInformation());
	playersInfoTable = new JTable(model);

	// To fix the sorting
	TableRowSorter<TableModel> sorter = new TableRowSorter<TableModel>(model);
	playersInfoTable.setRowSorter(sorter);
	playersInfoTable.setAutoCreateRowSorter(true);
	// We specify the element contained in the scrollable
	tableContainer.setViewportView(playersInfoTable);

	// Table that will show additional round information,inside a scrollpane
	// (in case we have a lot of players)
	JPanel infoContainer = new JPanel();
	infoContainer.setBackground(Color.LIGHT_GRAY);
	infoContainer.setLayout(new GridLayout(0, 1, 0, 0));
	getContentPane().add(tabbedPane, gbc_splitPane);

	JPanel additionalInfoContainer = new JPanel();
	additionalInfoContainer.setBackground(Color.LIGHT_GRAY);
	additionalInfoContainer.setLayout(new GridLayout(0, 1, 0, 0));
	infoContainer.add(additionalInfoContainer);
	JScrollPane scrollPane = new JScrollPane();
	additionalInfoContainer.add(scrollPane);
	additionalInfoTable = new JTable(getAdditionalTableModel(psi7_MainAg.getAdditionalInfo()));
	scrollPane.setViewportView(additionalInfoTable);

	// To show console output
	JScrollPane verboseContainer = new JScrollPane();
	verbosePane = new JTextArea();
	verboseContainer.setViewportView(verbosePane);

	// Build tabbed pane
	tabbedPane.addTab("Game Information", null, tableContainer, "Shows information about the whole game.");
	tabbedPane.addTab("Round Information", null, infoContainer, "Shows information about the current round.");
	tabbedPane.addTab("Verbose", null, verboseContainer, null);
	// Popup for right click on table
	final JPopupMenu popup = new JPopupMenu();

	JMenuItem moneyStats = new JMenuItem(SHOW_MONEY_STATS);
	JMenuItem winRatioStats = new JMenuItem(SHOW_WIN_STATS);
	JMenuItem roleStats = new JMenuItem(SHOW_ROLE_TATS);

	RightClickListener tableListener = new RightClickListener(popup);
	winRatioStats.addActionListener(tableListener);
	moneyStats.addActionListener(tableListener);
	roleStats.addActionListener(tableListener);
	popup.add(winRatioStats);
	popup.add(moneyStats);
	popup.add(roleStats);
	playersInfoTable.addMouseListener(tableListener);

	JPanel roundsInfoContainer = new JPanel();
	roundsInfoContainer.setBackground(Color.LIGHT_GRAY);
	GridBagConstraints gbc_roundsInfoContainer = new GridBagConstraints();
	gbc_roundsInfoContainer.insets = new Insets(0, 0, 5, 0);
	gbc_roundsInfoContainer.anchor = GridBagConstraints.SOUTH;
	gbc_roundsInfoContainer.gridwidth = 2;
	gbc_roundsInfoContainer.fill = GridBagConstraints.HORIZONTAL;
	gbc_roundsInfoContainer.gridx = 0;
	gbc_roundsInfoContainer.gridy = 1;
	getContentPane().add(roundsInfoContainer, gbc_roundsInfoContainer);
	roundsInfoContainer.setLayout(new BorderLayout(0, 0));

	JPanel progressBarContainer = new JPanel();
	roundsInfoContainer.add(progressBarContainer);
	gameProgressBar = new JProgressBar();
	progressBarContainer.setLayout(new BorderLayout(0, 0));

	gameProgressBar = new JProgressBar();
	gameProgressBar.setStringPainted(true);
	progressBarContainer.add(gameProgressBar);

	// Only new game option should be avaliable at start
	buttonContinue.setEnabled(false);
	buttonStop.setEnabled(false);
	setVisible(true);
    }

    /**
     * Returns the unique Instance of this {@link MainWindow}
     *
     * @return
     */
    public static final MainWindow getInstance() {
	return instance;
    }

    /**
     * Buffers this message. The buffer will be flushed on every
     *
     * @param message
     */
    public static void publish(String message) {
	logBuffer.add(message + "\n");
    }

    /**
     * Sets the main agent, so the window can call its methods.
     *
     * @param psi7Main
     */
    public void setMainAgent(Agent psi7Main) {
	mainAgent = (psi7_MainAg) psi7Main;
    }

    /**
     * Sets the status of the controllers to enabled /disabled. When the game is
     * being played, they will remain disabled to hint the user that no changes
     * will take effect. When one set of rounds ends, the controls will be
     * enabled again.
     *
     * @param state
     */
    public void setControlsActive(boolean state) {
	startingMoneySpinner.setEnabled(state);
	potRatioSpinner.setEnabled(state);
	fineFactorSpinner.setEnabled(state);
	moneyToPutSpinner.setEnabled(state);
	roundCounterSpinner.setEnabled(state);
	inspectorsSpinner.setEnabled(state);

    }

    /**
     * Used to update all the information on the {@link MainWindow}, every time
     * this method is called, the progressbar will be automatically updated.<br>
     * We only update tables around 3 times a second, to increase performance,
     * avoid flickering and avoid calling update methods from threads that are
     * not the Swing Dispatcher Thread.
     *
     * @throws InterruptedException
     * @throws InvocationTargetException
     */
    public void update() throws InvocationTargetException, InterruptedException {

	updateProgressBarStatus();

	// Update tables 3 times a second at most or if the MainAgent is paused,
	// this way we greatly improve performance and avoid flickering on
	// elements due to not updating them from dispatcher thread. This also
	// solves nasty exceptions due to how Swing handles threads.

	if (((System.currentTimeMillis() - lastUpdateTime) > 333) || (psi7_MainAg.getInstance().getPause())) {
	    final Runnable runnable = new Runnable() {
		@Override
		public void run() {
		    // Update player information
		    playersInfoTable.setModel(toTableModel(getPlayerInformation()));
		    // Update round information
		    additionalInfoTable.setModel(getAdditionalTableModel(psi7_MainAg.getAdditionalInfo()));
		    while (logBuffer.size() != 0)
			verbosePane.append(logBuffer.remove(0));
		    lastUpdateTime = System.currentTimeMillis();
		}
	    };
	    if (!SwingUtilities.isEventDispatchThread()) {

		SwingUtilities.invokeAndWait(runnable);
	    }

	}

    }

    /**
     * Sets buttons ready for a new game
     */
    public void gameHasFinished() {
	MainWindow.getInstance().butonNew.setEnabled(true);
	MainWindow.getInstance().buttonContinue.setEnabled(false);
	MainWindow.getInstance().buttonStop.setEnabled(false);
	MainWindow.getInstance().setControlsActive(true);

    }

    /**
     * Constructs a {@link TableModel} with information about a round. Usually
     * here we will show information about who inspected who, the role used by a
     * given player and so on
     *
     * @param additionalInfoMap
     *            information about a round
     * @return
     */
    private TableModel getAdditionalTableModel(HashMap<Integer, ArrayList<AdditionalInfo>> additionalInfoMap) {
	if (additionalInfoMap == null)
	    return new DefaultTableModel();
	// Creation of custom anonymous model, this way we block user editing
	// the table, wich could cause problems.
	Object[] columnNames = new Object[] { "ID", "Name", "Role", "Inspected by", "Inspected player", "Benefit" };
	DefaultTableModel model = new CustomTableModel(columnNames, 0);
	ArrayList<AdditionalInfo> arrayList = additionalInfoMap.get(PlayerInformation.getLastRound());
	if (arrayList == null)
	    return model;
	for (AdditionalInfo row : arrayList) {
	    model.addRow(row.toRow());
	}

	return model;
    }

    /**
     * Sets the value of the progressBar
     */
    private void updateProgressBarStatus() {
	long currentRound = PlayerInformation.getLastRound();
	long totalRounds = PlayerInformation.getTotalRounds();
	// Gets % of game completed
	long completed = (currentRound + 1) * 100 / (totalRounds);
	gameProgressBar.setValue((int) completed);
	// Estimate average time for a single round
	estimatedTimeAverage.compute((System.currentTimeMillis() - lastUpdate) * (totalRounds - currentRound));
	// Compute total time left from single round and total rounds
	roundSpeedAverag.compute(1 / ((System.currentTimeMillis() - lastUpdate) / 1000));
	// Format estimation to something human readable
	String timeLeftStimation = timeFormatter.format(new Date((long) (estimatedTimeAverage.currentAverage())));

	if (currentRound + 1 >= totalRounds) {
	    timeLeftStimation = timeFormatter.format(new Date((0)));
	}

	String textProgressRound = String.valueOf(currentRound + 1) + "/" + String.valueOf(totalRounds);
	gameProgressBar.setString(textProgressRound + " Time Left: " + timeLeftStimation + " @ "
		+ (int) roundSpeedAverag.currentAverage() + " rounds/s");
	lastUpdate = System.currentTimeMillis();

    }

    /**
     * Returns the players if they have changed, or null if no instance of the
     * {@link psi7_MainAg MainAgent} exists (And therefore no game) has started.
     *
     * @return
     */
    private static final HashMap<AID, PlayerInformation> getPlayerInformation() {
	if (psi7_MainAg.getInstance() != null)
	    return psi7_MainAg.getInstance().getPlayersInfo();
	else
	    return null;
    }

    /**
     * Dumps players onto a table
     *
     * @param map
     *            the map to dump, containing players
     * @return the {@link TableModel}, containing all data about the players.
     */
    private static final TableModel toTableModel(Map<?, ?> map) {
	if (map == null)
	    return new DefaultTableModel();
	// Creation of custom anonymous model, this way we block user editing
	// the table, wich could cause problems.
	Object[] columnNames = new Object[] { "ID", "Name", "Money", "Sucess %" };
	DefaultTableModel model = new CustomTableModel(columnNames, 0);

	// Populate the model with the desired values
	for (Map.Entry<?, ?> entry : map.entrySet()) {
	    PlayerInformation playerInfo = (PlayerInformation) entry.getValue();
	    double money = playerInfo.getMoneyAtRound().get(playerInfo.getMoneyAtRound().size() - 1);

	    double sucessRate = MathUtils.getWinRatio(playerInfo.getMoneyAtRound());
	    model.addRow(new Object[] { playerInfo.id, playerInfo.aid.getLocalName(),
		    new BigDecimal(money).setScale(2, RoundingMode.HALF_EVEN),
		    new BigDecimal(sucessRate).setScale(2, RoundingMode.HALF_EVEN).longValue() });
	}

	return model;
    }

    /**
     * Actions to perform on "Stop" press
     *
     * @author markoscl
     *
     */
    private static final class StopGameListener implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent arg0) {
	    try {
		MainWindow.getInstance().buttonContinue.setEnabled(true);
		MainWindow.getInstance().buttonStop.setEnabled(false);
		mainAgent.setPause(true);
		MainWindow.getInstance().update();// Updates window to last
						  // known state
	    } catch (Exception e) {
		e.printStackTrace();
	    }

	}
    }

    /**
     * Actions to perform on "Continue" press
     *
     * @author markoscl
     *
     */
    private static final class ContinueGameListener implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent arg0) {
	    try {

		MainWindow.getInstance().buttonStop.setEnabled(true);
		MainWindow.getInstance().buttonContinue.setEnabled(false);
		psi7_MainAg.getInstance().continueGame();

	    } catch (Exception e) {
		e.printStackTrace();

	    }

	}
    }

    /**
     * Encapsulates the actions to perform when the "About" dialog is pressed.
     *
     * @author markoscl
     *
     */
    private final static class AboutListener implements ActionListener {
	/**
	 * Opens the Window
	 */
	@Override
	public void actionPerformed(ActionEvent event) {
	    try {
		new AboutDialogCustom();
	    } catch (IOException e) {
		//
		e.printStackTrace();
	    }
	}

    }

    /**
     * Encapsulates the actions to perform when the "resetPlayer" button is
     * presed. Will reset all players statistics.
     *
     * @author markoscl
     *
     */
    private final static class resetPlayerListener implements ActionListener {

	/*
	 * (non-Javadoc)
	 *
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.
	 * ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
	    // Here we just inform the MainAgent that he must remove all
	    // player's stats. When the window wants to show them, they will be
	    // like at the beggining of the round
	    mainAgent.resetAllPlayers();
	}

    }

    private final static class CustomTableModel extends DefaultTableModel {
	@SuppressWarnings("rawtypes")
	final Class[] columns = new Class[] { Integer.class, String.class, BigDecimal.class, Integer.class };

	public CustomTableModel(Object[] columnNames, int rowCount) {
	    super(columnNames, rowCount);
	}

	/**
	 * To fix the sorting
	 * 
	 * @param columnIndex
	 * @return
	 */
	@Override
	public Class<?> getColumnClass(int columnIndex) {
	    if (columnIndex <= 3)
		return columns[columnIndex];
	    return String.class;
	}

	/**
	 * Avoid a table that we can edit.
	 */
	@Override
	public boolean isCellEditable(int i, int i1) {
	    return false;
	}
    }

    private final static class RightClickListener extends MouseAdapter implements ActionListener {
	JPopupMenu popup;

	public RightClickListener(JPopupMenu popup) {
	    this.popup = popup;
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	    if (!(MainWindow.getInstance().butonNew.isEnabled()
		    || MainWindow.getInstance().buttonContinue.isEnabled())) {
		return;
	    }
	    // Gets the source on the table
	    JTable source = (JTable) e.getSource();
	    int row = source.rowAtPoint(e.getPoint());
	    int column = source.columnAtPoint(e.getPoint());

	    if (!source.isRowSelected(row))
		source.changeSelection(row, column, false, false);
	    // Shows the popup on the location the user has clicked
	    popup.show(e.getComponent(), e.getX(), e.getY());

	}

	@Override
	public void actionPerformed(ActionEvent e) {

	    Component c = (Component) e.getSource();
	    JPopupMenu popup = (JPopupMenu) c.getParent();
	    JTable table = (JTable) popup.getInvoker();
	    int selectedRow = table.getSelectedRow();
	    int id = (Integer) table.getModel().getValueAt(table.convertRowIndexToModel(selectedRow), 0);

	    ChartPanel chart = null;
	    if (e.getActionCommand().equals(MainWindow.SHOW_MONEY_STATS)) {
		chart = PlayerStats.getMoneyChart("Money at Round",
			getPlayerInformation().get(psi7_MainAg.getInstance().getAIDfromID(id)),
			getPlayerInformation().get(psi7_MainAg.getInstance().getAIDfromID(id)).aid.getLocalName());

	    } else if (e.getActionCommand().equals(MainWindow.SHOW_WIN_STATS)) {
		chart = PlayerStats.getPieChart("Win rate %",
			getPlayerInformation().get(psi7_MainAg.getInstance().getAIDfromID(id)));
	    } else if (e.getActionCommand().equals(MainWindow.SHOW_ROLE_TATS)) {
		chart = PlayerStats.getRoleChart("Role stats",
			getPlayerInformation().get(psi7_MainAg.getInstance().getAIDfromID(id)));
	    }
	    if (chart == null)
		return;
	    JFrame statsFrame = new JFrame(
		    getPlayerInformation().get(psi7_MainAg.getInstance().getAIDfromID(id)).aid.getLocalName()
			    + " Stats");
	    statsFrame.getContentPane().add(chart);
	    statsFrame.setSize(500, 500);
	    statsFrame.setVisible(true);
	}

    }

    /**
     * @author marcos
     *
     */
    public static final class ChangeLogLevelListener implements ActionListener {

	/*
	 * (non-Javadoc)
	 *
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.
	 * ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {

	    CustomVerboseJDialog dialog = new CustomVerboseJDialog(logValues, "Verbose settings");
	    dialog.setSize(300, 300);
	    dialog.setVisible(true);
	}

    }

    /**
     * @author markoscl
     *
     */
    private static final class NewGameListener implements ActionListener {

	/*
	 * (non-Javadoc)
	 *
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.
	 * ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {

	    MainWindow.getInstance().butonNew.setEnabled(false);
	    MainWindow.getInstance().buttonContinue.setEnabled(false);
	    MainWindow.getInstance().buttonStop.setEnabled(true);
	    MainWindow.getInstance().setControlsActive(false);
	    // When new game is selected we get the values and start the game
	    int vTAM = (Integer) startingMoneySpinner.getValue();
	    double vR = (Double) potRatioSpinner.getValue();
	    int vNrounds = (Integer) roundCounterSpinner.getValue();
	    double vM = (Double) fineFactorSpinner.getValue();
	    double vX = (Double) moneyToPutSpinner.getValue();
	    int vNiMax = (Integer) inspectorsSpinner.getValue();
	    Level logLevel = Level.OFF;
	    System.out.println("Launching main agent with params: ");
	    System.out.println("TAM " + vTAM);
	    System.out.println("Pot ratio " + vR);
	    System.out.println("Nrounds " + vNrounds);
	    System.out.println("fineFactor " + vM);
	    System.out.println("MoneyToPut " + vX);
	    System.out.println("NInspectors " + vNiMax);
	    mainAgent.setParameters(vTAM, vR, vNrounds, vM, vX, vNiMax, logLevel);
	    mainAgent.startGame();

	}

    }

}
