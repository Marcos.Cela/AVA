import java.util.logging.Level;

@SuppressWarnings("serial")
public class psi7_FixedI extends Psi7Player {

	public psi7_FixedI(Level logLevel) {
		loggingLevel = logLevel;
	}

	public psi7_FixedI() {
		super();
	}

	@Override
	protected void setup() {
		type = Psi7Player.INSPECTOR;
		super.setup();
	}

	@Override
	protected void generateRole() {
		type = INSPECTOR;
	}
}
