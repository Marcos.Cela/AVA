import java.util.HashMap;
import java.util.Map.Entry;
import java.util.logging.Level;

/**
 * Utility class used to save information about the player that we have
 * observed/deducted info.
 * 
 * @author markoscl
 *
 */
class PlayerObservedInfo {
    int inspector = 0;
    int defector = 0;
    int cooperator = 0;
    Oraculo oraculo;

    /**
     * Returns the observed defect % of this player. Remember this information
     * is not 100% true and it is a rough stimation based on partial data.
     * 
     * @return
     */
    public double getDefectRatio() {
	if (defector == 0)
	    return 0;
	double total = inspector + defector + cooperator;
	return ((double) defector / total);

    }

    @Override
    public String toString() {
	return ("I: " + inspector + " D: " + defector + " C: " + cooperator + " RATIO: " + getDefectRatio());
    }

}

@SuppressWarnings("serial")
public class psi7_Intel extends Psi7Player {
    private static final double DEFECT_LOW_LIMIT = 0.3;
    /**
     * This will contain the ID of the player that is the best candidate to
     * inspect (High defect ratio)
     */
    int bestCandidate = -1;
    private HashMap<Integer, PlayerObservedInfo> playerInfo = new HashMap<Integer, PlayerObservedInfo>();

    Oraculo oraculo;

    /**
     * Creates a {@link psi7_Intel} player with the given logging {@link Level}.
     * 
     * @param logLevel
     */
    public psi7_Intel(Level logLevel) {
	loggingLevel = logLevel;
    }

    @Override
    protected void setup() {
	super.setup();
    }

    /**
     * Default constructor
     */
    public psi7_Intel() {
	super();
    }

    @Override
    protected int generatePlayerToInspect() {
	if (bestCandidate != -1)
	    return bestCandidate;
	return super.generatePlayerToInspect();
    }

    /**
     * This is where the inteligent agent will choose its next role.
     */
    @Override
    protected void generateRole() {
	/** On round 0 we gather information, and reset any remains */
	if (currentRound == 0) {
	    oraculo = new Oraculo(Np, TAM, x, r, m, NiMax, Nrounds);
	    playerInfo = new HashMap<Integer, PlayerObservedInfo>();
	    type = INSPECTOR;
	    return;
	}
	updateInformationFromLastRound();
	oraculo.updateResult(roundInformation.get(currentRound - 1));

	/**
	 * We dont want to perform any role variation if the main agents is
	 * asking our role for the same round (We will be acting based on the
	 * same info)
	 */
	if (isReplayRound)
	    return;
	else
	    type = oraculo.getBestRole();

	boolean debug = false;
	if (debug) {
	    // Print debug map:

	    System.out.println("------------------------------------------------------------------------------");
	    System.out.println("------------------------------------------------------------------------------");
	    System.out.println("------------------------------------------------------------------------------");
	    System.out.println("------------------------------------------------------------------------------");
	    System.out.println("------------------------------------------------------------------------------");
	    java.util.Iterator<Entry<Integer, PlayerObservedInfo>> iterator = playerInfo.entrySet().iterator();
	    while (iterator.hasNext()) {
		System.out.println("#############################");
		Entry<Integer, PlayerObservedInfo> entry = iterator.next();
		PlayerObservedInfo value = entry.getValue();
		System.out.println("For player: " + entry.getKey());
		System.out.println("Coop :" + value.cooperator);
		System.out.println("Def  :" + value.defector);
		System.out.println("Insp :" + value.inspector);
		System.out.println("-----Ratio: " + value.getDefectRatio() + " -----");

	    }
	}
    }

    /**
     * Updates info based on our last round
     */
    private void updateInformationFromLastRound() {
	if (type.equals(INSPECTOR)) {
	    iWasInspector();
	} else if (type.equals(DEFECTOR)) {
	    iWasDefector();
	} else
	    iWasCooperator();

    }

    private void iWasCooperator() {
	// TODO Auto-generated method stub

    }

    private void iWasDefector() {
	// TODO Auto-generated method stub

    }

    /**
     * When on the last round we were inspectors, save the information
     */
    private void iWasInspector() {
	bestCandidate = super.generatePlayerToInspect();
	if (true)
	    return;
	int lastRound = currentRound - 1;
	/** First we update the known inspectors */
	for (int inspectorID : inspectorsForRound.get(lastRound)) {
	    if (inspectorID != id) {
		PlayerObservedInfo info = playerInfo.containsKey(inspectorID) ? playerInfo.get(inspectorID)
			: new PlayerObservedInfo();
		info.inspector++;
		playerInfo.put(inspectorID, info);
	    }
	}
	System.out.println("**********************************************************************");
	System.out.println("Inspected: " + lastPlayerInspected);
	System.out.println("Benefit obtained: " + roundInformation.get(lastRound).benefit);
	// We won this round, so the inspected player was a defector
	if (roundInformation.get(lastRound).benefit > 0) {
	    System.out.println("DEFECTOR");
	    PlayerObservedInfo info = playerInfo.containsKey(lastPlayerInspected) ? playerInfo.get(lastPlayerInspected)
		    : new PlayerObservedInfo();
	    info.defector++;
	    playerInfo.put(lastPlayerInspected, info);
	} else {
	    System.out.println("COOPERATOR");
	    // The inspected player was a cooperator
	    PlayerObservedInfo info = playerInfo.containsKey(lastPlayerInspected) ? playerInfo.get(lastPlayerInspected)
		    : new PlayerObservedInfo();
	    info.cooperator++;
	    playerInfo.put(lastPlayerInspected, info);
	}
	System.out.println("**********************************************************************");

	// Now that we have updated information, we are going to save the best
	// player to inspect, who will be the one that has the more % of
	// defected rounds.Nothe this is an aproximation
	double bestRatio = 0.0d;
	int candidate = super.generatePlayerToInspect();// Random
	for (Integer identifier : playerInfo.keySet()) {
	    if (playerInfo.get(identifier).getDefectRatio() > bestRatio && identifier != id) {
		candidate = identifier;
		bestRatio = playerInfo.get(identifier).getDefectRatio();
	    }

	}
	bestCandidate = candidate;
	System.out.println("Best candidate is: " + bestCandidate + " with a defect rate of: " + bestRatio);

    }
}