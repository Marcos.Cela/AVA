import java.util.Random;
import java.util.logging.Level;

@SuppressWarnings("serial")
public class psi7_Random extends Psi7Player {
	/** {@link Random} to generate the Behaviour of this agent */
	private static final Random rand = new Random();

	public psi7_Random(Level logLevel) {
		loggingLevel = logLevel;
	}

	public psi7_Random() {
		super();
	}

	@Override
	protected void setup() {
		super.setup();
	}

	@Override
	protected void generateRole() {
		// Here we set the role that this agent is going to use this
		// round
		switch (rand.nextInt(3)) {
		case 0:
			type = Psi7Player.COOPERATOR;
			break;
		case 1:
			type = Psi7Player.DEFECTOR;
			break;
		case 2:
			type = Psi7Player.INSPECTOR;
			break;
		}
	}
}