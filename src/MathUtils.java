
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Math Utilities.
 */
public class MathUtils {
	/**
	 * Simple Moving Average implementation.
	 * 
	 * @author markoscl
	 *
	 */
	public static class SMA {
		/** List where we store all the values */
		private LinkedList<Double> values = new LinkedList<Double>();
		/** Lenght for this SMA */
		private int length;
		/** Sum for partial values */
		private double sum = 0;
		/** Avg value to return */
		private double average = 0;

		/**
		 * 
		 * @param length
		 *            the maximum length
		 */
		public SMA(int length) {
			if (length <= 0) {
				throw new IllegalArgumentException("Length must be greater than zero");
			}
			this.length = length;
		}

		/**
		 * Gets the current average
		 * 
		 * @return
		 */
		public double currentAverage() {
			return average;
		}

		/**
		 * Compute the moving average
		 * 
		 * @param value
		 *            The value
		 * @return The average
		 */
		public double compute(double value) {
			if (values.size() == length && length > 0) {
				sum -= ((Double) values.getFirst()).doubleValue();
				values.removeFirst();
			}
			sum += value;
			values.addLast(new Double(value));
			average = sum / values.size();
			return average;
		}
	}

	/**
	 * Rounds an object(Usually a {@link Number}) and returns a string
	 * representation of exactly 2 fraction digits.
	 * 
	 * @param decimalNumber
	 * @return
	 */
	public static final String roundTo2(Object decimalNumber) {
		NumberFormat nf = NumberFormat.getInstance();
		nf.setMaximumFractionDigits(2);
		nf.setMinimumFractionDigits(2);
		nf.setRoundingMode(RoundingMode.HALF_UP);

		return (nf.format(decimalNumber));
	}

	/**
	 * Based on a player's money at a given round, returns the rounds that the
	 * player has a benefit >0 , in a value normalized so the minimum is 0 and
	 * the max is 1. Higher values mean that the player has a high win rate
	 * (Rounds with benefit), and lower values means that the player has made a
	 * choice that gave 0 or negative winnings.
	 * 
	 * @param arrayList
	 *            The money at round, per round.
	 * @return winRate
	 */
	public static final double getWinRatio(ArrayList<Double> arrayList) {
		if (arrayList.size() < 2)
			return 0;
		/**
		 * Proceed to calculate the sucess rate based on the money the player
		 * has at any given round.
		 */
		int roundsWinning = 0;
		int roundsLoosing = 0;
		/**
		 * Ignore first value and start comparing round (n) to round (n-1) to
		 * find net benefit
		 */

		for (int i = 1; i < arrayList.size(); i++) {

			double lastValue = arrayList.get(i - 1);
			double actualValue = arrayList.get(i);
			if (actualValue - lastValue > 0) {
				roundsWinning++;
			} else {
				roundsLoosing++;
			}
		}
		/** Normalize these values */
		double winning = roundsWinning / ((double) roundsWinning + (double) roundsLoosing) * 100;
		return winning;
	}
}