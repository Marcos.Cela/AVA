import java.util.logging.Level;

@SuppressWarnings("serial")
public class psi7_FixedD extends Psi7Player {

	public psi7_FixedD(Level logLevel) {
		loggingLevel = logLevel;
	}
	public psi7_FixedD() {
		super();
	}
	@Override
	protected void setup() {
		type = Psi7Player.DEFECTOR;
		super.setup();
	}

	@Override
	protected void generateRole() {
		type = DEFECTOR;
	}
}