import java.security.InvalidParameterException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.AMSService;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.AMSAgentDescription;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.SearchConstraints;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;

/**
 * This class is intended to be extended by following players inside the PGG
 * game. Implements some convenience methods.
 *
 * @author markoscl
 *
 */
@SuppressWarnings({ "serial" })
public abstract class Psi7Player extends Agent {

    /* Possible values for type */
    /** Value of type for cooperators */
    public static final String COOPERATOR = "C";
    /** Value of type for inspectors */
    public static final String INSPECTOR = "I";
    /** Value of type for defectors */
    public static final String DEFECTOR = "D";
    /** This is the type of this player */
    protected String type;
    /** Global identifier for this agent */
    protected int id;
    /** Total numer of players on this game */
    protected int Np;
    /** Initial money for each player */
    protected double TAM;
    /** Money to put if the agent acts as Cooperator */
    protected double x;
    /** Number of rounds on this game */
    protected int Nrounds;
    /** Max number of inspector on this game */
    protected int NiMax;
    /** Fine ratio if the agent is caught acting as a defector */
    protected double m;
    /** Ratio of the pot */
    protected double r;
    /** Current round */
    protected int currentRound = 0;
    /** Value to check on intel player */
    protected boolean isReplayRound = false;
    /** {@link Logger} of this agent */
    protected Logger logger;
    /**
     * Instance used when we need random behaviour, usually when randomly
     * generating a player to inspect, or when a player must use some kind of
     * randomness on its role generation process
     */
    private final static Random rand = new Random();

    /** Logging level, default will be {@link Level Level.FINEST} */
    protected Level loggingLevel = Level.WARNING;

    /** Information about players acting as inspectors each round */
    protected HashMap<Integer, ArrayList<Integer>> inspectorsForRound = new HashMap<Integer, ArrayList<Integer>>();

    /** Information about the role of the players for each round */
    protected HashMap<Integer, RoundResult> roundInformation = new HashMap<Integer, RoundResult>();
    /** Contains the ID of the last player inspected, used in INTEL players */
    protected int lastPlayerInspected;

    /**
     * Cleans resources between games
     */
    private void clean() {
	currentRound = 0;
	inspectorsForRound = new HashMap<Integer, ArrayList<Integer>>();
	roundInformation = new HashMap<Integer, RoundResult>();
	isReplayRound = false;

    }

    /**
     * Since every player will use its own logic to generate its role, we must
     * override this method and correctly set it on every player. <br>
     * <b>Example:</b><br>
     * - A player with a fixed behaviour will just implement this method as:
     * <br>
     * <br>
     * <code> type = COOPERATOR;</code>
     */
    protected abstract void generateRole();

    /**
     * Implements a generic behaviour for all players, since they only differ in
     * the way they respond to the role message, or the way they generate the
     * player to inspect. These two behaviours should be overriden to make any
     * kind of player (Random, Fixed, intelligent...), and this behaviour should
     * be used to allow for easier agent's deployment.
     */
    protected void genericPlayerBehaviour() {

	// Receives startup mesasge with this agent ID and waits for game to
	// start

	receiveStartupMessage();
	while (currentRound < Nrounds) {
	    logger.log(Level.INFO, "Waiting for a new round message...");
	    receiveNewRoundMMessage();
	    while (true) {
		/**
		 * Generate role for every new round message, this way we can
		 * avoid being stuck if there are too may or too few inspectors,
		 * since agents with variable behaviour will eventually force
		 * this value to be valid.
		 */
		generateRole();
		logger.log(Level.INFO, "New round message received, waiting for Inspector role question...");
		receiveAndReplyInspectorRole();
		logger.log(Level.INFO,
			"Inspector role question has been replied! Waiting for [New Round/Inspector request]");

		/*
		 * Now we wait for either a Role request or a NewRound if
		 * inspector number does not complain.
		 */
		AID result = receiveNewRoundOrRoleRequest();
		if (result != null) {
		    replyToRoleRequest(result, generatePlayerToInspect());
		    logger.log(Level.INFO, "Received and replied to role request with {0}", type);
		    isReplayRound = false;
		    break;
		} else {
		    logger.log(Level.INFO,
			    "Message was a New Round message, since number of inspectors was not valid. Replaying...");
		    isReplayRound = true;
		}
	    }
	    logger.log(Level.INFO, "Waiting for round result message");
	    getGameResult();
	    logger.log(Level.INFO, "Round result message correctly received.");

	}
	logger.log(Level.SEVERE, "Game has finished, waiting for another game or platform shutdown...");
	clean();

    }

    /**
     * Searches for agents and returns a descriptions of the agents. The search
     * will be made using
     * {@link AMSService#search(Agent, AMSAgentDescription, SearchConstraints)
     * AMSService#search} , with no max results, and filtering only the
     * <b>rma</b>, <b>df</b> and <b>ams</b> agents.
     *
     * @return
     */
    protected static ArrayList<AMSAgentDescription> searchAgents(Agent searchAuthor) {
	ArrayList<AMSAgentDescription> agentDescriptions = new ArrayList<AMSAgentDescription>();
	AMSAgentDescription[] agents = null;
	try {
	    SearchConstraints constraints = new SearchConstraints();
	    constraints.setMaxResults(new Long(-1)); /// Do not use a max
	    agents = AMSService.search(searchAuthor, new AMSAgentDescription(), constraints);
	} catch (Exception e) {
	    e.printStackTrace();
	}

	for (AMSAgentDescription agent : agents) {
	    // Filter out the agents we dont care about and searchAuthor
	    if (!(agent.getName().getLocalName().equals("rma") || agent.getName().getLocalName().equals("df")
		    || agent.getName().getLocalName().equals("ams")
		    || agent.getName().getLocalName().equals(searchAuthor.getLocalName()))) {
		agentDescriptions.add(agent);
	    }

	}
	return agentDescriptions;

    }

    /**
     * Sends a message to an agent.
     *
     * @param type
     *            the Type of message, must be one of the known performatives
     *            and follow FIPA rules.
     * @param receiver
     *            the {@link AID} of the agent that will receive the message.
     * @param content
     *            content of the message.
     */
    protected void sendMessage(int type, AID receiver, String content) {
	ACLMessage acl = new ACLMessage(type);
	acl.addReceiver(receiver);
	acl.setContent(content);
	send(acl);
    }

    protected ACLMessage receiveNewRoundMMessage() {
	ACLMessage acl = blockingReceive();
	if (!(acl.getPerformative() == ACLMessage.INFORM) || !acl.getContent().equals("NewRound")) {
	    throw new InvalidParameterException("The New Round message was malformed,got: " + acl.toString());
	}
	return acl;

    }

    /**
     * Receives and parses the startup message, wich contains all game info
     * necessary to start it.
     *
     * @return
     */
    protected ACLMessage receiveStartupMessage() {
	logger.log(Level.INFO, "Waiting for startup message...");
	ACLMessage startupMessage = blockingReceive();

	Matcher matcher = Pattern
		.compile(
			"Id#(\\d+)#(\\d+),(\\d+[.]{0,1}\\d{0,50}),(\\d+[.]{0,1}\\d{0,50}),(\\d+[.]{0,1}\\d{0,50}),(\\d+[.]{0,1}\\d{0,50}),(\\d+),(\\d+)")
		.matcher(startupMessage.getContent());

	if (matcher.find()) {
	    id = Integer.parseInt(matcher.group(1));
	    Np = Integer.parseInt(matcher.group(2));
	    TAM = Double.parseDouble(matcher.group(3));
	    x = Double.parseDouble(matcher.group(4));
	    r = Double.parseDouble(matcher.group(5));
	    m = Double.parseDouble(matcher.group(6));
	    NiMax = Integer.parseInt(matcher.group(7));
	    Nrounds = Integer.parseInt(matcher.group(8));
	} else {
	    // Could not parse message, since we don`t even have a valid ID we
	    // are forced to close the game, try to do it as gracefully as
	    // possible.
	    throw new InvalidParameterException(
		    "The initial message provided by the Main Agent was malformed: " + startupMessage.getContent());
	}
	logger.log(Level.INFO, "Startup message received.");

	return startupMessage;
    }

    /**
     * Receives the message from the {@link psi7_MainAg} and replys to it. The
     * reply will vary if this {@link Agent} is acting as an inspector.
     */
    protected ACLMessage receiveAndReplyInspectorRole() {
	// Receives the message and checks if the message is correctly
	// formatted.
	ACLMessage acl = blockingReceive();
	if (!acl.getContent().equals("Inspector") || acl.getPerformative() != ACLMessage.REQUEST)
	    throw new InvalidParameterException(
		    "Expected ''Inspector'' on a REQUEST mesage, message was malformed: " + acl.toString());
	// Since the message is correctly formatted, build a reply and send it.
	ACLMessage reply = new ACLMessage(ACLMessage.INFORM);
	reply.setContent(type.equals(INSPECTOR) ? "Inspector#Yes" : "Inspector#No");
	reply.addReceiver(acl.getSender());
	send(reply);
	return acl;

    }

    /**
     * Receives the message from the {@link psi7_MainAg}. This message is either
     * the Role request for this round or the information of a new round if the
     * Agents that tried to act as inspectors were not a valid number of
     * inspectors.<br>
     * <b>They are restricted by:</b><br>
     * <code>( inspectors > 0 && inspectors < NiMax)</code>
     *
     * @returns <b>The main agent {@link AID}</b> when the message was a <b>Role
     *          Request</b> and the game must continue normally, null otherwise.
     */
    protected AID receiveNewRoundOrRoleRequest() {
	ACLMessage acl = blockingReceive();
	if (acl.getPerformative() == ACLMessage.INFORM) {
	    // Message was a new round must be started, return null to inform.
	    return null;
	}

	// Inspector number is OK for Main Agent, get inspector's for this round
	// and save them.
	if (acl.getPerformative() == (ACLMessage.REQUEST) && acl.getContent().startsWith("Role#")) {
	    // If message is >5 chars, then it contains inspectors.
	    if (acl.getContent().length() > 5) {
		// Parse inspectors and save info (for this round)
		String[] numbers = acl.getContent().substring(acl.getContent().indexOf("#") + 1).split("\\s*,\\s*");
		for (String number : numbers)
		    addNewInspectorForRound(currentRound, Integer.parseInt(number));

	    }

	} else {
	    // Message was malformed, inform the user and try to continue the
	    // game (assumes new round)
	    logger.log(Level.SEVERE,
		    "Message content was malformed, expected {INFORM}NewRound or {REQUEST}Role, but received message was :"
			    + acl.toString());
	    return null;
	}
	return acl.getSender();

    }

    /**
     * Adds a new inspector for a given round
     *
     * @param round
     * @param inspector
     */
    protected void addNewInspectorForRound(int round, int inspector) {
	if (inspectorsForRound.containsKey(round))
	    inspectorsForRound.get(round).add(inspector);
	else {
	    ArrayList<Integer> inspectorsIDs = new ArrayList<Integer>();
	    inspectorsIDs.add(inspector);
	    inspectorsForRound.put(round, inspectorsIDs);
	}
    }

    /**
     * Gets the game result message, that cointains the benefit for this round,
     * and the number of agents that acted as Contributors, Defectors and
     * Inspectors.
     *
     * @throws InvalidParameterException
     *             if the message is malformed.
     */
    protected ACLMessage getGameResult() {
	ACLMessage message = blockingReceive();
	// Checks mesage and inform if message is invalid
	if (message.getPerformative() != ACLMessage.INFORM || !message.getContent().startsWith("Benefit#")) {
	    throw new IllegalArgumentException(
		    "Expected { [ACLMessage.INFORM]->Benefit#X.X#Nc,Nd,Ni } but message was malformed:"
			    + message.toString());
	}
	// Message is valid, extract info
	Matcher matcher = Pattern.compile("Benefit#([-+]{0,1}\\d+\\.{0,1}\\d{0,100})#(\\d+),(\\d+),(\\d+)")
		.matcher(message.getContent());

	if (matcher.find()) {
	    double benefit = Double.parseDouble(matcher.group(1));
	    int Nc = Integer.parseInt(matcher.group(2));
	    int Nd = Integer.parseInt(matcher.group(3));
	    int Ni = Integer.parseInt(matcher.group(4));
	    // Save info
	    roundInformation.put(currentRound, new RoundResult(Nc, Nd, Ni, benefit));

	} else {
	    throw new InvalidParameterException("Benefits message malformed.");
	}
	// We just finished a round
	currentRound++;
	return message;
    }

    /**
     * Reply to the role request, and informs (if we are an inspector) of the
     * player that we want to inspect.
     *
     * @param idOfPlayerToInspect
     */
    protected void replyToRoleRequest(AID mainAgent, int idOfPlayerToInspect) {
	// Because message was valid, we send the Main Agent our intentions
	// for this round.
	ACLMessage reply = new ACLMessage(ACLMessage.INFORM);
	reply.addReceiver(mainAgent);
	String replyContent;
	// Inform the MA of our intentions for this round (Or for this current
	// "try" if the last "try" was not valid due to invalid number of
	// inspectors (Have to replay round).
	replyContent = type.equals(INSPECTOR) ? ("MyRole#I#" + idOfPlayerToInspect) : ("MyRole#" + type);
	reply.setContent(replyContent);
	send(reply);
    }

    /**
     * Randomly generates an id of a player that is NOT an inspector. Since this
     * method will be only useful when we are also inspectors, we will never
     * inspect ourselves (We are contained in the INSPECTORS message received
     * from the MA).
     *
     * @return random ID of a player that is NOT an inspector (For this round).
     */
    protected int generatePlayerToInspect() {
	// Function only allowed to inspectors
	if (!type.equals(INSPECTOR))
	    return -1;
	// Gets inspectors for this round
	ArrayList<Integer> inspectors = inspectorsForRound.get(currentRound);

	int id = rand.nextInt(Np);
	// If no players are acting as inspectors, accept the first value
	if (inspectors == null || inspectors.isEmpty()) {
	    lastPlayerInspected = id;
	    return id;
	}
	while (inspectors.contains(id)) {

	    id = rand.nextInt(Np);
	}
	lastPlayerInspected = id;
	return id;
    }

    /**
     * initializes the logger
     */
    protected void setLogger() {
	logger = Logger.getLogger(getLocalName());
	logger.setUseParentHandlers(false);// Remove other handlers
	logger.setLevel(loggingLevel);
	final ConsoleHandler handler = new ConsoleHandler();// To output text to
							    // console
	final CustomHandler cHandler = new CustomHandler();// To output text to
							   // our GUI
	final CustomLogFormatter formatter = new CustomLogFormatter(getLocalName());
	cHandler.setFormatter(formatter);
	handler.setFormatter(formatter);
	logger.addHandler(handler);
	logger.addHandler(cHandler);
    }

    @Override
    protected void setup() {
	setLogger();

	try {
	    logger.setLevel(Level.parse(getArguments()[0].toString()));
	} catch (Exception e) {
	    logger.setLevel(Level.OFF);

	}
	logger.log(Level.SEVERE, "Hello, i am a player with name: {0}, I am ready! I am using log level: {1}",
		new Object[] { getAID().getName(), logger.getLevel().toString() });
	DFAgentDescription agentDesc = new DFAgentDescription();
	agentDesc.setName(getAID());
	ServiceDescription serviceDesc = new ServiceDescription();
	// Set type and name to "Player" for compatibility with Yellow Pages
	serviceDesc.setType("Player");
	serviceDesc.setName("Player");
	agentDesc.addServices(serviceDesc);

	try {
	    DFService.register(this, agentDesc);

	} catch (FIPAException fe) {
	    fe.printStackTrace();
	}

	addBehaviour(new genericBehaviour(this));

    }

    @Override
    protected void takeDown() {

	try {
	    // Clears all messages for me
	    while (receive() != null) {
		// Nothing...
	    }

	    DFService.deregister(this);
	} catch (FIPAException e) {
	    e.printStackTrace();
	}

    }

    /**
     * This class will implement the common behaviour to ANY kind of player.
     * <br>
     * Since the only difference between players are the role for a given round,
     * and the player to inspect (if acting as inspector), we just have to
     * implement this differences in 2 methods.<br>
     * Using this behaviour and overriding {@link Psi7Player#generateRole()} and
     * {@link Psi7Player#generatePlayerToInspect()} will give us an easy and
     * fast way to implement ANY player.
     * 
     * @author markoscl
     *
     */
    protected class genericBehaviour extends CyclicBehaviour {

	public genericBehaviour(Psi7Player player) {
	    super(player);
	}

	@Override
	public void action() {
	    genericPlayerBehaviour();

	}

    }
}

/**
 * 
 * @author marcos
 *
 */
final class CustomHandler extends Handler {

    @Override
    public void publish(LogRecord record) {

    }

    @Override
    public void flush() {
    }

    @Override
    public void close() throws SecurityException {
    }

}

/**
 * @author marcos
 *
 */
final class CustomLogFormatter extends Formatter {
    private final String agentName;
    /** Time format for logger */
    private final static SimpleDateFormat timeformatter = new SimpleDateFormat("hh:mm:ss:SSS");

    public CustomLogFormatter(String name) {
	agentName = name;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.util.logging.Formatter#format(java.util.logging.LogRecord)
     */
    @Override
    public String format(final LogRecord record) {
	StringBuilder b = new StringBuilder();
	b.append("[");
	b.append(timeformatter.format(System.currentTimeMillis()));
	b.append(" ");
	b.append(agentName);
	b.append("] ");
	b.append(formatMessage(record));
	b.append(System.getProperty("line.separator"));
	return b.toString();
    }

}

/**
 * Small container for round result analysis
 *
 * @author markoscl
 *
 */
final class RoundResult {
    /** Number of players that acted as cooperators for this round */
    int cooperators;
    /** Number of players that acted as defectors for this round */
    int defectors;
    /** Number of players that acted as inspectors for this round */
    int inspectors;
    /** Benefit for this round */
    double benefit;

    /**
     * Creates a container with the given parameters.
     *
     * @param nCooperators
     *            Number of cooperators for a round.
     * @param nDefectors
     *            Number of defectors for a round.
     * @param nInspectors
     *            Number of inspectors for a round.
     * @param roundBenefit
     *            Profit for a round.
     */
    public RoundResult(int nCooperators, int nDefectors, int nInspectors, double roundBenefit) {
	cooperators = nCooperators;
	defectors = nDefectors;
	inspectors = nInspectors;
	benefit = roundBenefit;
    }
}
