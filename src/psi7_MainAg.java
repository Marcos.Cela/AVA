import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import javax.swing.SwingUtilities;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.AMSAgentDescription;
import jade.lang.acl.ACLMessage;

@SuppressWarnings("serial")
public class psi7_MainAg extends Agent {
	// assumes the current class is called logger
	private final static Logger logger = Logger.getLogger(psi7_MainAg.class.getName());;
	/** Time format for logger */
	private final static SimpleDateFormat timeformatter = new SimpleDateFormat("hh:mm:ss:SSS");

	/** The logging level of this agent */
	private static Level logLevel;
	/**
	 * Instance of this class, it is a singleton, cause we will never have more
	 * than one
	 */
	private static psi7_MainAg instance;
	/** To populate the additional info table */
	private static HashMap<Integer, ArrayList<AdditionalInfo>> additionalInfo = new HashMap<Integer, ArrayList<AdditionalInfo>>();
	/** Contains information about each player */
	private HashMap<AID, PlayerInformation> playerInformationList = new HashMap<AID, PlayerInformation>();

	/** The current round for this game */
	private Integer currentRound = 0;
	/** Total number of agents playing this game */
	private int numberOfPlayers = 0;
	/**
	 * ID to send to the agents. The main agent serves this ID on the first
	 * message, increments this field and sends another agent its ID until all
	 * registered agents have a valid unique ID.
	 */
	private int lastIdServed = 0;
	/** Total number of players */
	private int Np;
	/** Initial total amount of money a player has */
	private int TAM;
	/** Amout of money to put in the pot by each player */
	private double x;
	/** Interest factor */
	private double r;
	/** Fine factor */
	private double m;
	/** Maximum number of players that can act as inspectors */
	private int NiMax;
	/** Number of rounds to be played */
	private int Nrounds;
	/** Agent representation of this {@link psi7_MainAg} */
	Agent thisAgent = this;
	/** Will be true only when the game is paused */
	public boolean pause = false;
	private boolean waitingForGameStart;

	private ArrayList<AMSAgentDescription> agentDescriptions;

	/**
	 * initializes the logger
	 *
	 * @param level
	 */
	private void setLogger(Level level) {
		logger.setUseParentHandlers(false);// Remove other handlers
		logger.setLevel(level);
		ConsoleHandler handler = new ConsoleHandler();
		CustomHandler cHandler = new CustomHandler();
		Formatter formatter = new Formatter() {
			@Override
			public String format(LogRecord arg0) {
				StringBuilder b = new StringBuilder();
				b.append("[");
				b.append(timeformatter.format(System.currentTimeMillis()));
				b.append(" ");
				b.append(getLocalName());
				b.append("] ");
				b.append(formatMessage(arg0));
				b.append(System.getProperty("line.separator"));
				return b.toString();
			}
		};
		handler.setFormatter(formatter);
		cHandler.setFormatter(formatter);
		logger.addHandler(handler);
		logger.addHandler(cHandler);
		logger.log(Level.SEVERE, "Logger correctly set, using log level: {0}", level.toString());
	}

	@Override
	protected void setup() {
		instance = this;
		setLogger(Level.INFO);
		logger.setLevel(Level.INFO);
		logger.log(Level.INFO, "Main agent AID is: {0}", getAID().getLocalName());
		Np = Psi7Player.searchAgents(getInstance()).size();
		// Creates a window on event dispatch thread
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				MainWindow window = new MainWindow();
				window.setMainAgent(thisAgent);
				window.setVisible(true);

			}
		});

		addBehaviour(new MainAgentBehaviour(this));

	}

	@Override
	protected void takeDown() {
		try {
			DFService.deregister(this);
		} catch (FIPAException e) {
		}

		logger.log(Level.INFO, "Agent {0} is terminating.", getAID().getLocalName());
	}

	/**
	 * @return the playersInfo
	 */
	public HashMap<AID, PlayerInformation> getPlayersInfo() {
		return playerInformationList;
	}

	/**
	 * Increments the lastId this Main Agent has served and returns a string
	 * representation of the generated ID.
	 *
	 * @return string representation of the generated ID.
	 */
	private String generateUniqueId() {
		String val = Integer.toString(lastIdServed);
		lastIdServed++;
		return val;
	}

	/**
	 * Returns the internal identifier for this AID.
	 *
	 * @param agentAID
	 *            the AID of the agent on wich we are performing the search. The
	 *            value is considered to exist.
	 * @return the internal ID associated for this AID or -1 if no ID for that
	 *         agent is found.
	 */
	public int getIDfromAID(AID agentAID) {
		for (PlayerInformation pi : playerInformationList.values()) {
			if (pi.aid.equals(agentAID)) {
				return pi.id;
			}
		}
		return -1;
	}

	/**
	 * Returns the AID associated to its internal ID.
	 *
	 * @param id
	 *            the internal ID of the agent.
	 * @return the AID of the agent with that specified id
	 */
	public AID getAIDfromID(int id) {
		for (PlayerInformation pi : playerInformationList.values()) {
			if (pi.id == id) {
				return pi.aid;
			}
		}
		return null;
	}

	/**
	 * Constructs a roundInformation for this round.
	 */
	private RoundInformation constructRoundInfo() {
		RoundInformation roundInformation = new RoundInformation();
		Iterator<Entry<AID, PlayerInformation>> iterator = playerInformationList.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<AID, PlayerInformation> entry = iterator.next();
			PlayerInformation playerInfo = entry.getValue();
			String role = playerInfo.getRole();
			if (role.equals(Psi7Player.COOPERATOR))
				roundInformation.numberOfContributors++;
			else if (role.equals(Psi7Player.DEFECTOR))
				roundInformation.numberOfDefectors++;
			else if (role.equals(Psi7Player.INSPECTOR))
				roundInformation.numberOfInspectors++;
		}
		return roundInformation;
	}

	/**
	 * Sets a pause
	 *
	 * @param param
	 */
	public void setPause(boolean param) {
		logger.log(Level.FINE, "Game will be paused on next round.");
		pause = param;
	}

	/**
	 * We dont have to follow the singleton pattern, because the agent will
	 * always be initialized from {@link jade.Boot}, and at least a single
	 * instance will be avaliable. Also if the platform that houses this agent
	 * is not running, this method will return null.
	 *
	 * @return The instance of the main Agent (singleton) or null if platform is
	 *         not running.
	 */
	public static psi7_MainAg getInstance() {
		return instance;
	}

	class MainAgentBehaviour extends CyclicBehaviour {

		public MainAgentBehaviour(psi7_MainAg psi7_MainAg) {
			super(psi7_MainAg);
		}

		@Override
		public void action() {
			// Search for players and stores them
			agentDescriptions = Psi7Player.searchAgents(thisAgent);
			waitingForGameStart = true;
			blockingReceive();// Wait for game start
			/** Once game has started set PlayerInformation static values */
			/** Game has started */
			waitingForGameStart = false;
			// Set auxiliar parameters (used to initialize arrays and lists to a
			// suitable size and avoid runtime growth as much as possible).
			PlayerInformation.setParams(TAM, Nrounds);
			/** Sends game information message */

			playGame();
			logger.log(Level.INFO, "Game has finished...");
			currentRound = 0;
			MainWindow.getInstance().gameHasFinished();
			resetStats();
			// Finish this behaviour
		}

		/**
		 * Resets statistics between rounds
		 */
		private void resetStats() {
			lastIdServed = 0;
			additionalInfo = new HashMap<Integer, ArrayList<AdditionalInfo>>();
			PlayerInformation.resetCommonValues();

		}

		private void playGame() {
			logger.log(Level.INFO, "New game has started, sending startup message to all agents.");
			numberOfPlayers = sendStartupMessage();
			logger.log(Level.INFO, "Startup message successfully sent to {0} agents", numberOfPlayers);
			boolean validInspectors = false;
			while (currentRound < Nrounds) {// Game is not finished yet
				if (pause) {
					blockingReceive();
					pause = false;
				}
				validInspectors = false;
				while (!validInspectors) {// Ni invalid, retry
					/** Sends New Round Information */

					// Clear registered inspectors (if any)
					PlayerInformation.clearInspectorsForRound(currentRound);
					logger.log(Level.INFO, "There are still rounds to be played ({0}) Sending new round message...",
							(Nrounds - currentRound));
					sendMessageToAllAgents(ACLMessage.INFORM, "NewRound");
					/** Ask wich players are going to act as inspectors */
					logger.log(Level.INFO, "Asking players if they are acting as inspectors...");
					/** Sends request to players acting as inspectors */
					sendMessageToAllAgents(ACLMessage.REQUEST, "Inspector");

					int inspectorsThisRound = receiveInspectorsReply();
					if (inspectorsThisRound <= 0 || inspectorsThisRound > NiMax) {

						validInspectors = false;
					} else {
						logger.log(Level.INFO, "Valid number of inspectors.");

						validInspectors = true;
					}
				}
				// Now we send a role request for every player that is not
				// acting as an inspector:

				AskRoleAndInformInspectors();

				logger.log(Level.INFO, "Waiting from role message from all players...");
				receiveRolesFromPlayers();
				logger.log(Level.INFO, "Roles have been received.");

				// Now the players are waiting for the benefit message,first we
				// resolve the game and get the messages that we have to send to
				// each agent
				logger.log(Level.INFO, "Calculating round outcome...");
				// Calculate round outcome for every player, inform the players
				// and save info
				calculateRound();

				if (logger.isLoggable(Level.INFO))
					printGameAndPlayersInfo();

				PlayerInformation.setRoundInformation(constructRoundInfo());
				PlayerInformation.setLastRound(currentRound);

				currentRound++;

				try {
					MainWindow.getInstance().update();
				} catch (Exception e) {
					e.printStackTrace();

				}
			}

		}

		/**
		 * En este m�todo comprobar que para currentRound
		 */
		private void printGameAndPlayersInfo() {
			int lastRound = currentRound;
			logger.log(Level.FINER, "##########################   ROUND {0} RESUME   ##########################",
					lastRound);
			logger.log(Level.FINER, "Inspectors at this round:");
			for (AID inspector : PlayerInformation.getInspectorsAtRound(lastRound)) {
				logger.log(Level.FINER, "\t Inspector ID: {0}   Inspector name: {1}",
						new Object[]{getIDfromAID(inspector), inspector.getLocalName()});
			}

			logger.log(Level.FINER, "Players being inspected at this round:");
			for (AID player : PlayerInformation.playersInspectedByRound.get(lastRound).keySet()) {
				logger.log(Level.FINER, "Player ID: {0}  Player name: {1}",
						new Object[]{getIDfromAID(player), player.getLocalName()});
				for (AID inspector : PlayerInformation.playersInspectedByRound.get(lastRound).get(player))
					logger.log(Level.FINER, "\t\tInspected by Inspector ID: {0} Inspector name: {1}",
							new Object[]{getIDfromAID(inspector), inspector.getLocalName()});

			}
			logger.log(Level.FINER, "Total players with ID and role:");
			Iterator<Entry<AID, PlayerInformation>> iterator = playerInformationList.entrySet().iterator();
			while (iterator.hasNext()) {
				Entry<AID, PlayerInformation> entry = iterator.next();
				Integer playerID = entry.getValue().id;
				PlayerInformation playerInfo = entry.getValue();
				logger.log(Level.FINER, "Player  ID: {0} name: {1} was acting as {2}",
						new Object[]{playerID, playerInfo.aid.getLocalName(), playerInfo.getRole()});
			}
			logger.log(Level.FINER, "######################################################################");

		}

		private void AskRoleAndInformInspectors() {
			// Build messages for inspectors and others
			ACLMessage inspectorsMessage = null;
			ACLMessage othersMessage = new ACLMessage(ACLMessage.REQUEST);
			othersMessage.setContent("Role#");
			// Get inspectors
			ArrayList<AID> inspectors = PlayerInformation.getInspectorsAtRound(currentRound);
			// Stringbuilder for faster string concatenation
			if (inspectors != null) {
				inspectorsMessage = new ACLMessage(ACLMessage.REQUEST);
				StringBuilder builder = new StringBuilder();
				builder.append("Role#");
				for (int i = 0; i < inspectors.size(); i++) {
					builder.append(getIDfromAID(inspectors.get(i)));
					if (i != inspectors.size() - 1)
						builder.append(",");
				}
				// Inform all inspectors of other inspectors ID's
				inspectorsMessage.setContent(builder.toString());
			}

			// Send a message to all agents, and if the agent is an inspector
			// then inform about inspectors this round
			ArrayList<AMSAgentDescription> agentDescriptions = Psi7Player.searchAgents(thisAgent);
			for (AMSAgentDescription description : agentDescriptions) {
				if (inspectors != null && inspectors.contains(description.getName())) {
					// Agent is an inspector, notify other inspectors
					fastSend(inspectorsMessage, description.getName());
				} else {
					// Agent is a defector or cooperator
					fastSend(othersMessage, description.getName());
				}

			}

		}

		/**
		 * Calculates the outcome of a round, and generates messages for each
		 * type of player
		 */
		private void calculateRound() {
			ArrayList<AdditionalInfo> additionalInfoTable = new ArrayList<AdditionalInfo>();
			logger.log(Level.FINE, "***************  DETAILED ROUND INFORMATION  ****************************");
			ArrayList<AID> cooperators = new ArrayList<AID>();
			ArrayList<AID> defectors = new ArrayList<AID>();
			ArrayList<AID> inspectors = new ArrayList<AID>();
			// Resolve roles of players
			int Nc = 0;
			int Nd = 0;
			int Ni = 0;
			// Count players
			Iterator<Map.Entry<AID, PlayerInformation>> iterator = playerInformationList.entrySet().iterator();
			while (iterator.hasNext()) {
				Map.Entry<AID, PlayerInformation> entryPair = iterator.next();
				String role = entryPair.getValue().getRole();
				// Update values and add agents to a list
				if (role.equals(Psi7Player.COOPERATOR)) {
					Nc++;
					cooperators.add(entryPair.getKey());
				} else if (role.equals(Psi7Player.DEFECTOR)) {
					Nd++;
					defectors.add(entryPair.getKey());
				} else if (role.equals(Psi7Player.INSPECTOR)) {
					Ni++;
					inspectors.add(entryPair.getKey());
				}

			}

			double Bc = (x * Nc * r) / ((double) Nc + (double) Nd) - x;
			// Construct messages for contributors
			ACLMessage contMessage = new ACLMessage(ACLMessage.INFORM);
			contMessage.setContent("Benefit#" + Bc + "#" + Nc + "," + Nd + "," + Ni);
			// Adds the cooperators to the message and saves their info
			for (AID cooperator : cooperators) {
				fastSend(contMessage, cooperator);
				playerInformationList.get(cooperator).addBenefit(Bc);
				additionalInfoTable
						.add(new AdditionalInfo(Psi7Player.COOPERATOR, getIDfromAID(cooperator), cooperator, null, Bc));

				logger.log(Level.FINE, "Cooperator {0} receives {1} ", new Object[]{cooperator.getLocalName(), Bc});
			}

			// Now perform math for money on defectors and inspectors
			// If the player being inspected is a defector we calculate how much
			// inspectors are inspecting it, and add
			// the message of the defector and the message of the inspectors
			// Otherwise, the inspectors receive 0 and we calculate how much for
			// each defector

			// Ammount of money for each defector not inspected
			double Bd = (x * Nc * r) / ((double) Nc + (double) Nd);
			// For defectors that are inspected
			double BdInspected = -1 * m * Bd;

			/** Message sent to those defectors that are not inspected */
			ACLMessage notInspectedACL = new ACLMessage(ACLMessage.INFORM);
			notInspectedACL.setContent("Benefit#" + Bd + "#" + Nc + "," + Nd + "," + Ni);
			/** Message sent to those defectors that are inspected */
			ACLMessage inspectedACL = new ACLMessage(ACLMessage.INFORM);
			inspectedACL.setContent("Benefit#" + BdInspected + "#" + Nc + "," + Nd + "," + Ni);

			if (!PlayerInformation.playersInspectedByRound.containsKey(currentRound)) {
				// No players were inspected, all defectors receive max profit
				for (AID defectorNotCaught : defectors) {
					fastSend(notInspectedACL, defectorNotCaught);
					playerInformationList.get(defectorNotCaught).addBenefit(Bd);
					additionalInfoTable.add(new AdditionalInfo(Psi7Player.DEFECTOR, getIDfromAID(defectorNotCaught),
							defectorNotCaught, null, Bd));

				}
			} else {

				HashMap<AID, HashSet<AID>> playersInspected = PlayerInformation.playersInspectedByRound
						.get(currentRound);
				// Money of defector that has been inspected
				for (AID defector : defectors) {
					if (playersInspected.containsKey(defector)) {

						// Defector has been caught
						fastSend(inspectedACL, defector);
						playerInformationList.get(defector).addBenefit(BdInspected);
						logger.log(Level.FINE, "Defector({0}) has been inspected, he receives: {1}",
								new Object[]{defector.getLocalName(), BdInspected});

						additionalInfoTable.add(new AdditionalInfo(Psi7Player.DEFECTOR, getIDfromAID(defector),
								defector, null, BdInspected));
						/*
						 * Here the player has been inspected, we gather
						 * information about all inspectors, and if those
						 * inspectors are inspecting THIS player, we add it
						 */

					} else {
						// Defector was not inspected, we send him his benefit
						fastSend(notInspectedACL, defector);
						playerInformationList.get(defector).addBenefit(Bd);
						logger.log(Level.FINE, "Defector({0}) has not been inspected, he receives: {1}",
								new Object[]{defector.getLocalName(), Bd});
						additionalInfoTable.add(
								new AdditionalInfo(Psi7Player.DEFECTOR, getIDfromAID(defector), defector, null, Bd));

					}
				}

				/**
				 * Now for every player being inspected, if he is a defector we
				 * split the fine between the inspectors
				 */

				ACLMessage noProfitACL = new ACLMessage(ACLMessage.INFORM);
				noProfitACL.setContent("Benefit#0#" + Nc + "," + Nd + "," + Ni);
				// Iterate over all the players being inspected
				Iterator<Map.Entry<AID, HashSet<AID>>> iter = playersInspected.entrySet().iterator();
				while (iter.hasNext()) {
					Map.Entry<AID, HashSet<AID>> entry = iter.next();
					AID playerBeingInspected = entry.getKey();
					HashSet<AID> playersInspecting = entry.getValue();

					if (defectors.contains(playerBeingInspected)) {
						// Player is a defector,divide profit between inspectors
						ACLMessage profitACL = new ACLMessage(ACLMessage.INFORM);
						double profit = (Bd * (1.0000d + m)) / playersInspecting.size();
						profitACL.setContent("Benefit#" + profit + "#" + Nc + "," + Nd + "," + Ni);
						for (AID inspect : playersInspecting) {
							fastSend(profitACL, inspect);
							playerInformationList.get(inspect).addBenefit(profit);
							logger.log(Level.FINE,
									"Inspector ({0}), with ID ({1}) inspected player ({2}) who was a defector, inspector obtains: {3}",
									new Object[]{inspect.getLocalName(), getIDfromAID(inspect),
											playerBeingInspected.getLocalName(), profit});
							// Find this player in the list of additional Info,
							// and add the inspector inspecting him
							for (AdditionalInfo addInf : additionalInfoTable) {
								if (addInf.getAID().equals(playerBeingInspected)) {
									addInf.addPlayerInspectingMe(Integer.toString(getIDfromAID(inspect)));
								}
							}
							additionalInfoTable.add(new AdditionalInfo(Psi7Player.INSPECTOR, getIDfromAID(inspect),
									inspect, playerBeingInspected.getLocalName(), profit));
						}
					} else {
						for (AID inspect : playersInspecting) {
							fastSend(noProfitACL, inspect);
							playerInformationList.get(inspect).addBenefit(0.00d);
							logger.log(Level.FINE,
									"Inspector ({0}), with ID ({1}) inspected player ({2}) who was not a defector",
									new Object[]{inspect.getLocalName(), getIDfromAID(inspect),
											playerBeingInspected.getLocalName()});
							// Find this player in the list of additional Info,
							// and add the inspector inspecting him
							for (AdditionalInfo addInf : additionalInfoTable) {
								if (addInf.getAID().equals(playerBeingInspected)) {
									addInf.addPlayerInspectingMe(Integer.toString(getIDfromAID(inspect)));
								}
							}
							additionalInfoTable.add(new AdditionalInfo(Psi7Player.INSPECTOR, getIDfromAID(inspect),
									inspect, playerBeingInspected.getLocalName(), 0.0d));

						}

					}
				}
			}

			logger.log(Level.FINE, "#####################   END OF ROUND {0}  #######################", currentRound);
			additionalInfo.put(currentRound, additionalInfoTable);
			return;
		}

		/**
		 * Receives the role that each agent is using for this round, and the
		 * player that each inspector is going to inspect.
		 */
		private void receiveRolesFromPlayers() {
			int messagesReceived = 0;
			while (messagesReceived < numberOfPlayers) {
				logger.log(Level.FINER, "Receiving role reply {0}/{1}",
						new Object[]{messagesReceived, numberOfPlayers - 1});
				ACLMessage reply = blockingReceive();
				logger.log(Level.FINEST, "Message received: {0}", reply.toString());
				if (reply != null && reply.getPerformative() == ACLMessage.INFORM
						&& reply.getContent().startsWith("MyRole#I#")) {

					// Save this player as an inspector
					PlayerInformation playerInformation = playerInformationList.get(reply.getSender());
					playerInformation.addRole(Psi7Player.INSPECTOR);
					playerInformationList.put(reply.getSender(), playerInformation);

					// The player is acting as inspector
					int playerBeingInspected = Integer.parseInt(reply.getContent()
							.substring(reply.getContent().lastIndexOf("#") + 1, reply.getContent().length()));
					// Save the player that this inspector is inspecting
					// already has info for this round
					if (PlayerInformation.playersInspectedByRound.containsKey(currentRound)) {
						// There are players being inspected on this round
						// Inspectors inspecting this player
						if (PlayerInformation.playersInspectedByRound.get(currentRound)
								.containsKey(getAIDfromID(playerBeingInspected))) {
							// Add this inspector to the list of inspectors
							// inspecting the player
							PlayerInformation.playersInspectedByRound.get(currentRound)
									.get(getAIDfromID(playerBeingInspected)).add(reply.getSender());
						} else {
							// There are no inspectors inspecting this
							// player yet
							// Set this inspector as the first inspector
							// inspecting this agent
							HashSet<AID> inspectors = new HashSet<AID>();
							inspectors.add(reply.getSender());
							PlayerInformation.playersInspectedByRound.get(currentRound)
									.put(getAIDfromID(playerBeingInspected), inspectors);

						}
					} else {
						// There are no players being inspected for this
						// round, add this player to the list of inspected
						// players
						HashMap<AID, HashSet<AID>> playersBeingInspected = new HashMap<AID, HashSet<AID>>();
						// Add the player being inspected and the inspector
						// inspecting it
						HashSet<AID> inspectors = new HashSet<AID>();
						inspectors.add(reply.getSender());
						playersBeingInspected.put(getAIDfromID(playerBeingInspected), inspectors);
						PlayerInformation.playersInspectedByRound.put(currentRound, playersBeingInspected);

					}

				} else if (reply != null) {
					// The player is not acting as an inspector, get the role
					String role = reply.getContent().substring(reply.getContent().indexOf("#") + 1,
							reply.getContent().length());
					// Save the role of the player for this round
					PlayerInformation playerInformation = playerInformationList.get(reply.getSender());
					playerInformation.addRole(role);
					playerInformationList.put(reply.getSender(), playerInformation);

				}

				messagesReceived++;
			}
		}

		/**
		 * Sends an startup message to all players, informing about the game
		 * parameters, and their ID. Saves the relation AID<->ID
		 *
		 * @return the number of messages sent (Number of agents)
		 */
		private int sendStartupMessage() {
			// Searches all players on the platform
			ArrayList<AMSAgentDescription> agentDescriptions = Psi7Player.searchAgents(thisAgent);
			// Proceeds to inform all players of the parameters of the new game
			for (AMSAgentDescription description : agentDescriptions) {
				if (description.getName().equals(getName()))
					continue;

				ACLMessage acl = new ACLMessage(ACLMessage.INFORM);
				acl.setInReplyTo(thisAgent.getName());
				acl.addReceiver(description.getName());
				// Sends a message correctly formatted following the specs. A
				// new ID is generated for every agent.
				String id = generateUniqueId();
				acl.setContent(
						"Id#" + id + "#" + Np + "," + TAM + "," + x + "," + r + "," + m + "," + NiMax + "," + Nrounds);
				send(acl);
				logger.log(Level.FINEST, "Sending startup message: {0}", acl.toString());
				// Saves the information sent
				PlayerInformation playerInfo = new PlayerInformation(Integer.parseInt(id), description.getName());
				playerInformationList.put(description.getName(), playerInfo);
			}
			return agentDescriptions.size();

		}

		/**
		 * Receives messages from the players acting as inspectors.
		 *
		 * @throws InterruptedException
		 */
		private int receiveInspectorsReply() {
			int numberOfInspectors = 0;
			int messagesReceived = 0;
			while (messagesReceived < numberOfPlayers) {
				ACLMessage message = blockingReceive();
				if (message != null && message.getContent().contains("Yes")) {
					// The player replied with "YES", he is acting as inspector
					// this round
					logger.log(Level.FINER, "Received reply from inspector");
					logger.log(Level.FINEST, "Message received: {0}", message.toString());
					numberOfInspectors++;
					PlayerInformation.addInspectorForRound(currentRound, message.getSender());
				}

				messagesReceived++;
			}
			return numberOfInspectors;

		}

		/**
		 * Sends a message to every agent in our system. Agents will be gathered
		 * with {@link Psi7Player#searchAgents(Agent)}
		 *
		 * @param perf
		 *            The performative of the message to send
		 * @param message
		 *            The message to send
		 */
		private void sendMessageToAllAgents(int perf, String message) {
			ACLMessage acl = new ACLMessage(perf);
			acl.setContent(message);
			// Proceeds to inform all players of the parameters of the new game
			for (AMSAgentDescription agentDescription : agentDescriptions) {
				AID agentAID = agentDescription.getName();
				// I dont want to send the message to myself
				if (agentAID.equals(getName()))
					continue;
				// send my message
				fastSend(acl, agentAID);
			}
		}

		/**
		 * Sends a message to an agent. This method is used when we want to send
		 * the same message to multiple agents.<br>
		 * The reason for this method is that it is <b>much much</b> faster than
		 * creating a single {@link ACLMessage} and adding multiple receivers
		 * with {@link ACLMessage#addReceiver(AID)} then sending it, since the
		 * AMS has to copy the message once for each receiver making it slower.
		 *
		 * @param acl
		 *            the message to send
		 * @param name
		 *            the receiver of this message
		 */
		private void fastSend(ACLMessage acl, AID name) {
			acl.addReceiver(name);
			send(acl);
			acl.removeReceiver(name);
			logger.log(Level.FINEST, "Sending message: {0}", acl.toString());
		}

	}

	/**
	 * Sets the parameters for this game, initializing the agent's values and
	 * the {@link PlayerInformation} static values.
	 *
	 * @param vTAM
	 *            The initial ammount of money on each plaer
	 * @param vR
	 *            The rate we use to multiply by the pot ammount
	 * @param vNrounds
	 *            Number of rounds to play
	 * @param vM
	 *            fine factor
	 * @param vX
	 *            ammount of money that each player puts to play
	 * @param vNiMax
	 *            max number of inspectors
	 * @param level
	 *            LogLevel, as defined in {@link Logger}
	 */
	public void setParameters(int vTAM, double vR, int vNrounds, double vM, double vX, int vNiMax, Level level) {
		TAM = vTAM;
		r = vR;
		Nrounds = vNrounds;
		m = vM;
		x = vX;
		NiMax = vNiMax;
		logLevel = level;
		PlayerInformation.setTotalRounds(Nrounds);
		logger.setLevel(logLevel);
	}

	/**
	 * This will send a message to the {@link psi7_MainAg MainAgent}, who is
	 * waiting for the new game message. When the agent receives this message he
	 * will start the game.
	 */
	public void startGame() {
		/**
		 * Send the start message only when the game has not started, otherwise
		 * this will result in random behaviour or system crash
		 */
		if (!waitingForGameStart)
			return;
		ACLMessage startMessage = new ACLMessage(ACLMessage.INFORM);
		startMessage.addReceiver(getAID());
		send(startMessage);

	}

	/**
	 *
	 * @return pause
	 */
	public boolean getPause() {
		return pause;
	}

	/**
	 * Sets this {@link psi7_MainAg MainAgent} logger to the specified
	 * {@link Level}
	 */
	public static void setLogLevel(Level lev) {
		logger.setLevel(lev);
		logger.log(Level.SEVERE, "Main agent log is now set to {0}", logger.getLevel().toString());
	}

	/**
	 * @return the additionalInfo
	 */
	public static HashMap<Integer, ArrayList<AdditionalInfo>> getAdditionalInfo() {
		return additionalInfo;
	}

	/**
	 * @param additionalInfo
	 *            the additionalInfo to set
	 */
	public static void setAdditionalInfo(HashMap<Integer, ArrayList<AdditionalInfo>> additionalInfo) {
		psi7_MainAg.additionalInfo = additionalInfo;
	}

	/**
	 * If the game is paused, commands the MA to resume it
	 */
	public void continueGame() {
		if (pause) {
			ACLMessage startMessage = new ACLMessage(ACLMessage.INFORM);
			startMessage.addReceiver(getAID());
			send(startMessage);
		} else {
			logger.log(Level.INFO, "You can't continue the game if you are already running it!");
		}

	}

	/**
	 * Resets all player's stats
	 */
	public void resetAllPlayers() {
		for (PlayerInformation playerInfo : playerInformationList.values()) {
			playerInfo.reset();
		}
	}

	public boolean isPaused() {
		return pause;
	}

}

class AdditionalInfo {
	private final int playerID;
	private final AID playerAID;
	private ArrayList<String> playersInspectingMe = new ArrayList<String>();
	private final String playerInspected;
	private final double benefitForRound;
	private final String type;

	public AdditionalInfo(String playerType, int id, AID aid, String inspected, double benefit) {
		type = playerType;
		playerID = id;
		playerAID = aid;
		playerInspected = inspected;
		benefitForRound = benefit;
	}

	public AID getAID() {
		return playerAID;
	}

	public void addPlayerInspectingMe(String inspector) {
		playersInspectingMe.add(inspector);
	}

	/**
	 * Returns a row view of this object
	 *
	 * @return
	 */
	public Object[] toRow() {
		Object val = playersInspectingMe.size() == 0 ? new String("") : Arrays.toString(playersInspectingMe.toArray());

		return new Object[]{playerID, playerAID.getLocalName(), type, val, playerInspected, benefitForRound};
	}

}

/**
 * Simple container for round information
 *
 * @author markoscl
 *
 */
final class RoundInformation {
	public int numberOfInspectors;
	public int numberOfDefectors;
	public int numberOfContributors;
}
