import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import jade.core.AID;

/**
 * This class is a container with some extra functions. Its main purpose is to
 * save information about a player, such as the internal ID of the player, its
 * {@link AID}, the money that the player has for a given round...
 * 
 * <br>
 * It also saves some statics containing general game information, not related
 * to a single agent. This values are, for example, players inspected on a round
 * or players acting as inspectors for a round.
 * 
 * @author markoscl
 *
 */
final class PlayerInformation {
	/* Static values, these values are common to all players */
	/**
	 * Contains information about the players that are being inspected each
	 * round, and who is inspecting them.
	 */
	public static HashMap<Integer, HashMap<AID, HashSet<AID>>> playersInspectedByRound = new HashMap<Integer, HashMap<AID, HashSet<AID>>>();
	/** Saves all inspectors for every round */
	private static HashMap<Integer, ArrayList<AID>> inspectorsAtRound = new HashMap<Integer, ArrayList<AID>>();
	/** Saves round info, per round */
	private static RoundInformation roundInfo;
	/** Initial money for each player */
	private static double initialMoney;
	/** Last round data */
	private static int lastRound;
	/** Total number of rounds */
	private static int totalRounds;

	/**
	 * Values of each player associated with this PlayerInformation
	 */
	/** Internal identifier for this player */
	public int id;
	/** {@link AID} identifier for this player */
	public AID aid;
	/** Role of the player for each round */
	private ArrayList<String> roleAtRound = new ArrayList<String>();
	/** Benefit of the player for each round */
	private ArrayList<Double> benefitAtRound = new ArrayList<Double>();
	/** Money for every round */
	private ArrayList<Double> moneyAtRound = new ArrayList<Double>();

	/**
	 * Set some basic parameters
	 * 
	 * @param initialTAM
	 * @param totalRounds
	 */
	public static void setParams(int initialTAM, int totalRounds) {
		initialMoney = initialTAM;
	}

	/**
	 * Resets static values
	 */
	public static void resetCommonValues() {
		playersInspectedByRound = new HashMap<Integer, HashMap<AID, HashSet<AID>>>(totalRounds);
		inspectorsAtRound = new HashMap<Integer, ArrayList<AID>>(totalRounds);
		roundInfo = null;
	}

	/**
	 * Creates a {@link PlayerInformation}
	 * 
	 * @param aid2
	 * @param i
	 */
	public PlayerInformation(int internalID, AID agentAID) {
		id = internalID;
		aid = agentAID;
	}

	/*
	 * Clears all registered inspectors for a round. This method is useful when
	 * the main agent has registered these inspectors, but there are too many
	 * inspectors or too few. Therefore we need to reset those nispectors,
	 * because in this new round they may change.
	 */
	public static void clearInspectorsForRound(Integer currentRound) {
		inspectorsAtRound.remove(currentRound);
	}

	/**
	 * Sets the additional round information for the last round
	 * 
	 * @param info
	 */
	public static void setRoundInformation(RoundInformation info) {
		roundInfo = info;

	}

	/**
	 * Sets the parameter total rounds
	 * 
	 * @param nrounds
	 */
	public static void setTotalRounds(int nrounds) {
		totalRounds = nrounds;
	}

	/**
	 * Provides access to the list of inspectors for a given round
	 *
	 * @return the inspectorsatround
	 */
	public static ArrayList<AID> getInspectorsAtRound(int round) {
		return inspectorsAtRound.get(round);
	}

	/**
	 * Adds a benefit to this player.
	 * 
	 * @param bc
	 */
	public void addBenefit(double bc) {
		benefitAtRound.add(bc);// Add the benefit at that round

	}

	/**
	 * Generates and returns the money at a given round for this player.
	 *
	 */
	public ArrayList<Double> getMoneyAtRound() {
		moneyAtRound = new ArrayList<Double>();
		// On first index always start with initial money
		moneyAtRound.add(initialMoney);
		double money = initialMoney;

		for (Double ben : benefitAtRound) {
			moneyAtRound.add(money + ben);
			money += ben;
		}

		return moneyAtRound;

	}

	/**
	 * Adds a player acting as an inspector for a given round
	 *
	 * @param round
	 *            current round
	 * @param inspector
	 *            {@link AID} of the inspector
	 */
	public static void addInspectorForRound(int round, AID inspector) {
		if (inspectorsAtRound.containsKey(round)) {
			inspectorsAtRound.get(round).add(inspector);
		} else {
			ArrayList<AID> inspectors = new ArrayList<AID>();
			inspectors.add(inspector);
			inspectorsAtRound.put(round, inspectors);
		}
	}

	/**
	 * @return the benefitAtRound
	 */
	public ArrayList<Double> getBenefitAtRound() {
		return benefitAtRound;
	}

	/**
	 * @return the lastRound
	 */
	public static int getLastRound() {
		return lastRound;
	}

	/**
	 * @param lastRound
	 *            the lastRound to set
	 */
	public static void setLastRound(int lastRound) {
		PlayerInformation.lastRound = lastRound;

	}

	public static int getTotalRounds() {
		return totalRounds;
	}

	/**
	 * @return the roundinfo
	 */
	public static RoundInformation getRoundinfo() {
		return roundInfo;
	}

	/**
	 * Resets information
	 */
	public void reset() {
		roleAtRound = new ArrayList<String>(totalRounds);
		moneyAtRound = new ArrayList<Double>(totalRounds);
		benefitAtRound = new ArrayList<Double>(totalRounds);
	}

	/**
	 * Returns last known role for this player
	 * 
	 * @return
	 */
	public String getRole() {
		return roleAtRound.get(roleAtRound.size() - 1);
	}

	/**
	 * 
	 * @return registered values of this player's roles
	 */
	public ArrayList<String> getRoleAtRound() {
		return roleAtRound;
	}

	/**
	 * Adds a role for this player
	 * 
	 * @param role
	 *            the role for this player
	 */
	public void addRole(String role) {
		roleAtRound.add(role);
	}

}
