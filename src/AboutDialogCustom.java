import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

/**
 * Creates a panel with information about this project's Author, Account name
 * and Linkedin Profile and link.
 *
 * @author Markoscl
 */
@SuppressWarnings("serial")
public class AboutDialogCustom extends JDialog {
	/** Main container for all the elements */
	private final JPanel contentPanel = new JPanel();

	/**
	 * Creates the about dialog for this game
	 *
	 * @throws IOException
	 */
	public AboutDialogCustom() throws IOException {
		/**
		 * Behavior on Close, we selected to dispose, this dialog is not meant
		 * to be viewed all the time
		 */
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		// Icon set to info
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(AboutDialogCustom.class.getResource("/com/sun/java/swing/plaf/motif/icons/Inform.gif")));
		setMinimumSize(new Dimension(600, 400));
		setTitle("About");
		// Visibility mode
		setModalityType(ModalityType.APPLICATION_MODAL);
		setBackground(Color.LIGHT_GRAY);
		setBounds(100, 100, 600, 400);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBackground(Color.LIGHT_GRAY);
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		/*
		 * Make the panel a grid with two rows and 1 column. First row is for
		 * general information and second row is for LinkedIn image
		 */
		contentPanel.setLayout(new GridLayout(2, 1, 0, 0));
		// Text panel for author information
		JTextPane textPane = new JTextPane();
		textPane.setAlignmentY(Component.TOP_ALIGNMENT);
		StyledDocument doc = textPane.getStyledDocument();
		SimpleAttributeSet center = new SimpleAttributeSet();
		// Center the text so it looks pleasing
		StyleConstants.setAlignment(center, StyleConstants.ALIGN_CENTER);
		doc.setParagraphAttributes(0, doc.getLength(), center, false);
		textPane.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		textPane.setBackground(Color.LIGHT_GRAY);
		// Accent support is added via Unicode chars
		textPane.setText("Author:\t\t\t\tMarcos Cela L\u00F3pez\r\n\r\ne-mail:\t\t\t\tmarcoscela1@gmail.com");
		textPane.setEditable(false);// User does not need to mess around with
									// this
		contentPanel.add(textPane);
		// Text panel that will fo
		JLabel picLabel = new JLabel(new ImageIcon("Resources\\Logo-2CRev-121px-TM.png"));
		// Change cursor to hand to indicate that you can click on it
		picLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		// Picture size
		picLabel.setPreferredSize(new Dimension(500, 200));
		picLabel.setMinimumSize(new Dimension(500, 200));
		picLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		/**
		 * We add a MouseListener that will prompt the user with the Author's
		 * Linkedin Profile when clicked
		 */
		picLabel.addMouseListener(new LinkedInOnClick());
		picLabel.setMaximumSize(new Dimension(500, 200));
		picLabel.setLabelFor(picLabel);
		picLabel.setToolTipText("You can find more info about me here!");
		contentPanel.add(picLabel);
		// Buttons container and buttons
		JPanel buttonPane = new JPanel();
		buttonPane.setBackground(Color.LIGHT_GRAY);
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		JButton okButton = new JButton("OK");
		okButton.addMouseListener(new DisposeOnClick());
		okButton.setBackground(Color.GRAY);
		buttonPane.add(okButton);
		getRootPane().setDefaultButton(okButton);// Allows using "Enter" to
													// press "OK"

		JButton cancelButton = new JButton("Cancel");
		cancelButton.setBackground(Color.GRAY);
		cancelButton.addMouseListener(new DisposeOnClick());
		buttonPane.add(cancelButton);
		setVisible(true);

	}

	/**
	 * Class used on buttons that will dispose window when clicked.
	 * 
	 * @author markoscl
	 *
	 */
	class DisposeOnClick extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent event) {
			dispose();
		}
	}

	/**
	 * Class used on the LinkedIn Image. Will open a new browser (Or use an
	 * existing one) to take the user to the author's LinkedIn Profile.
	 * 
	 * @author markoscl
	 *
	 */
	class LinkedInOnClick extends MouseAdapter {
		@Override
		public void mouseClicked(MouseEvent event) {
			// First we chick if the user is on a supported environment and if
			// the user can browse or not
			Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
			if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
				try {
					// Launch the browse command
					desktop.browse(new URI("https://es.linkedin.com/in/marcoscelalopez"));// //$NON-NLS-0$
				} catch (Exception e) {

					System.out.println("Unexpected error when trying to open LinkedIn URL.");
					e.printStackTrace();
				}
			}
		}
	}

}
