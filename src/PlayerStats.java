import java.util.ArrayList;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 *
 * @author markoscl
 *
 */
public class PlayerStats {
	/**
	 * This class encapsulates all the information about a chart.
	 * 
	 * @author markoscl
	 *
	 */

	/**
	 * Creates a {@link ChartPanel}Panel containing a {@link JFreeChart} wich
	 * backs up a {@link PieDataset}. The chart contained contains therefore a
	 * pie-chart style graph representing the % of won and lost rounds by this
	 * player.
	 *
	 * @param title
	 *            Title used
	 * @param playerInfo
	 *            the relation of rounds(index) and money at that round(value at
	 *            that index).
	 * @param playerName
	 *            the playerName of wich this <b>moneyAtRound</b> is.
	 * @return a {@link ChartPanel} containing a {@link JFreeChart}with a
	 *         pie-chart like style.
	 */
	public static ChartPanel getPieChart(final String title, final PlayerInformation playerInfo) {
		JFreeChart chart = createPieChart(createPieDataset(playerInfo.getMoneyAtRound()), title);
		return new ChartPanel(chart);
	}

	/**
	 * Creates a {@link ChartPanel} containing a {@link JFreeChart} wich backs
	 * up a {@link XYDataset}. The chart contained contains herefore a function
	 * like style chart, representing the money of that player at a given round.
	 *
	 * @param title
	 *            Title used
	 * @param playerInformation
	 *            the relation of rounds(index) and money at that round(value at
	 *            that index).
	 * @param playerName
	 *            the playerName of wich this <b>moneyAtRound</b> is.
	 * @return a {@link ChartPanel} containing a {@link JFreeChart}with a
	 *         function like style.
	 */
	public static ChartPanel getMoneyChart(final String title, final PlayerInformation playerInformation,
			final String playerName) {
		XYDataset dataset = createDataset(playerInformation.getMoneyAtRound(), title);
		final JFreeChart chart = ChartFactory.createXYLineChart(playerName + " Money", "Round", "Money", dataset,
				PlotOrientation.VERTICAL, true, true, false);
		ChartPanel chartPanel = new ChartPanel(chart);
		return chartPanel;
	}

	/**
	 * 
	 * @param string
	 * @param playerInformation
	 * @return
	 */
	public static ChartPanel getRoleChart(final String title, final PlayerInformation playerInformation) {
		JFreeChart chart = createPieChart(createRolePieDataset(playerInformation.getRoleAtRound()), title);
		return new ChartPanel(chart);
	}

	private static PieDataset createRolePieDataset(ArrayList<String> roleAtRound) {
		/**
		 * Calculate roles
		 */
		int roundsAsInspector = 0;
		int roundsAsDefector = 0;
		int roundsAsCooperator = 0;

		for (int i = 1; i < roleAtRound.size(); i++) {
			if (roleAtRound.get(i).equals(Psi7Player.INSPECTOR)) {
				roundsAsInspector++;
			} else if (roleAtRound.get(i).equals(Psi7Player.COOPERATOR)) {
				roundsAsCooperator++;
			} else {
				roundsAsDefector++;
			}
		}
		/** Normalize these values */
		Double total = ((double) roundsAsInspector + (double) roundsAsCooperator + (double) roundsAsDefector) * 100;
		Double inspector = roundsAsInspector / total;
		Double defector = roundsAsDefector / total;
		Double cooperator = roundsAsCooperator / total;
		DefaultPieDataset dataset = new DefaultPieDataset();
		if (roundsAsInspector > 0)
			dataset.setValue("Inspector", inspector);
		if (roundsAsDefector > 0)
			dataset.setValue("Defector", defector);
		if (roundsAsCooperator > 0)
			dataset.setValue("Cooperator", cooperator);
		return dataset;
	}

	/**
	 * Creates a {@link JFreeChart} from a given {@link PieDataset}
	 *
	 * @param dataset
	 *            the {@link PieDataset} from wich the {@link JFreeChart} is
	 *            created.
	 * @param title
	 *            the title for this {@link JFreeChart}
	 * @return
	 */
	private static JFreeChart createPieChart(final PieDataset dataset, final String title) {

		JFreeChart chart = ChartFactory.createPieChart(title, // chart
																// title
				dataset, // data
				true, // include legend
				true, false);

		PiePlot plot = (PiePlot) chart.getPlot();
		plot.setNoDataMessage("No data available");
		plot.setCircular(false);
		plot.setLabelGap(0.05);// 5% gap between chart and labels
		return chart;

	}

	/**
	 * Creates a {@link PieDataset} containing a relation of won/lost rounds for
	 * a given relation of money and rounds.
	 *
	 * @param arrayList
	 *            contains the relation of rounds(index) and money(value for
	 *            that index(round)).
	 * @return a {@link PieDataset} containing the relation of won/lost rounds.
	 */
	private static PieDataset createPieDataset(final ArrayList<Double> arrayList) {
		double winRatio = MathUtils.getWinRatio(arrayList) / 100;
		DefaultPieDataset dataset = new DefaultPieDataset();
		dataset.setValue("Win", winRatio);
		dataset.setValue("Lose", (1.000d - winRatio));
		return dataset;
	}

	/**
	 *
	 * @param arrayList
	 *            contains the relation of rounds(index) and money(value for
	 *            that index(round)).
	 * @return a {@link XYDataset} created from the <b>moneyAtRound </b>
	 *         parameter.
	 */
	private static XYDataset createDataset(final ArrayList<Double> arrayList, final String title) {
		final XYSeries player1 = new XYSeries(title);
		// Populate dataset with money
		for (int i = 0; i < arrayList.size(); i++) {
			player1.add(i, arrayList.get(i));
		}
		final XYSeriesCollection collection = new XYSeriesCollection();
		collection.addSeries(player1);
		return collection;
	}

}
