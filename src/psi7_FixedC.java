import java.util.logging.Level;

@SuppressWarnings("serial")
public class psi7_FixedC extends Psi7Player {
	public psi7_FixedC(Level logLevel) {
		loggingLevel = logLevel;
	}

	public psi7_FixedC() {
		super();
	}

	@Override
	protected void setup() {
		type = Psi7Player.COOPERATOR;
		super.setup();
	}

	@Override
	protected void generateRole() {
		type = COOPERATOR;
	}
}