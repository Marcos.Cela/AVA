import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JTextPane;

import jade.core.AID;

/**
 *
 */

/**
 * @author Markoscl
 *
 */
@SuppressWarnings("serial")
public class CustomVerboseJDialog extends JDialog {

	@SuppressWarnings({"rawtypes", "unchecked"})
	public CustomVerboseJDialog(String[] levels, String title) {
		setTitle(title);
		getContentPane().setBackground(Color.LIGHT_GRAY);

		JComboBox combo = new JComboBox();
		combo.setToolTipText("Highly deatailed logs will cause the system to run slower");
		combo.setModel(new DefaultComboBoxModel(levels));
		getContentPane().add(combo, BorderLayout.CENTER);
		combo.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				JComboBox combo = (JComboBox) e.getSource();
				CustomActionListenerPlayers.setLevel(Level.parse(combo.getSelectedItem().toString()));
				CustomActionListenerAgent.setLevel(Level.parse(combo.getSelectedItem().toString()));

			}
		});

		JButton players = new JButton("Players");
		players.addActionListener(new CustomActionListenerPlayers(Level.parse(combo.getSelectedItem().toString())));
		getContentPane().add(players, BorderLayout.WEST);

		JButton mainAgent = new JButton("Main Agent");
		mainAgent.addActionListener(new CustomActionListenerAgent(Level.parse(combo.getSelectedItem().toString())));
		getContentPane().add(mainAgent, BorderLayout.EAST);
		JTextPane verboseTxt = new JTextPane();
		verboseTxt.setEditable(false);
		verboseTxt.setBackground(Color.LIGHT_GRAY);
		verboseTxt.setText("Set the desired logging level and select the target to apply");
		getContentPane().add(verboseTxt, BorderLayout.NORTH);

	}

}

final class CustomActionListenerPlayers implements ActionListener {
	private static Level logLevel = Level.INFO;

	public CustomActionListenerPlayers(Level lev) {
		logLevel = lev;
	}
	public static void setLevel(Level lev) {
		logLevel = lev;
	}
	@Override
	public void actionPerformed(ActionEvent e) {

		HashMap<AID, PlayerInformation> playersInfo = psi7_MainAg.getInstance().getPlayersInfo();
		for (AID player : playersInfo.keySet()) {
			Logger logger = Logger.getLogger(player.getLocalName());
			if (true) {
				logger.setLevel(logLevel);
				System.out.println("Changed log level of: " + logger.getName() + " to : " + logger.getLevel());
			}

		}

	}

}

/**
 * @author Markoscl
 *
 */
final class CustomActionListenerAgent implements ActionListener {

	private static Level level = Level.INFO;

	public CustomActionListenerAgent(Level parse) {
		level = parse;
	}
	public static void setLevel(Level lev) {
		level = lev;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.
	 * ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		psi7_MainAg.setLogLevel(level);

	}

}
