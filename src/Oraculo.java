import java.util.ArrayList;
import java.util.List;

/**
 * 
 */

/**
 * 
 * {@link Oraculo} is a class that, based on past info will suggest the best
 * role for our Intelligent player.<br>
 * The calculations are based on the expected benefit of playing a given role
 * for a given round.<br>
 * <br>
 * The way of choosing the best role is as follows:<br>
 * <br>
 * First, and based on the last <b>x%</b> of the total rounds played, we
 * calculate the roles distribution.<br>
 * For example, for a game with <b>N</b> rounds, we base our calculations on the
 * last:<br>
 * <code><b>N / x</b></code><br>
 * Rounds played, so we can quickly adapt to possible changes.
 * 
 * <br>
 * <br>
 * Then, based on the roles distribution and the given ratios (Pot ratio, fine
 * ratio...) and other parameters, we choose the role that should have the
 * biggest expected benefit, based on previously played rounds.
 * 
 * @author markoscl
 *
 *
 */
public class Oraculo {
    /**
     * This value will be used to scale down the number of rounds that are used
     * in calculating the best role. Higher values mean slower response to
     * change, so we don't want it very big. Suppose 10, so we make calculations
     * based on the last 10% of total rounds played
     */
    private final int ELEMENTS;
    /** Total number of players */
    private int Np;
    /** Initial total amount of money a player has */
    private double TAM;
    /** Amout of money to put in the pot by each player */
    private double x;
    /** Interest factor */
    private double r;
    /** Fine factor */
    private double m;
    /** Maximum number of players that can act as inspectors */
    private int NiMax;
    /** Number of rounds to be played */
    private int Nrounds;
    private List<Integer> defectors = new ArrayList<Integer>();
    private List<Integer> cooperators = new ArrayList<Integer>();
    private List<Integer> inspectors = new ArrayList<Integer>();

    public Oraculo(int Np, double tAM2, double x, double r, double m, int NiMax, int Nrounds) {
	this.Nrounds = Nrounds;
	this.Np = Np;
	TAM = tAM2;
	this.x = x;
	this.r = r;
	this.m = m;
	this.NiMax = NiMax;
	ELEMENTS = this.Nrounds / 10;

    }

    public void updateResult(RoundResult roundResult) {
	// Remove oldest entry
	if (defectors.size() >= ELEMENTS)
	    defectors.remove(0);
	if (cooperators.size() >= ELEMENTS)
	    cooperators.remove(0);
	if (inspectors.size() >= ELEMENTS)
	    inspectors.remove(0);

	// Add newest
	defectors.add(roundResult.defectors);
	inspectors.add(roundResult.inspectors);
	cooperators.add(roundResult.cooperators);
    }

    public String getBestRole() {
	// By default perform the only role that has a worse case of non
	// negative profit.
	String bestRole = Psi7Player.INSPECTOR;
	// Calculate the tendency of roles
	double defN = 0;
	double coopN = 0;
	double inspN = 0;
	for (Integer count : defectors) {
	    defN += count;
	}
	for (Integer count : cooperators) {
	    coopN += count;
	}
	for (Integer count : inspectors) {
	    inspN += count;
	}
	// Scale values
	defN = ((double) defN / defectors.size());
	coopN = ((double) coopN / cooperators.size());
	inspN = ((double) inspN / inspectors.size());
	System.out.println("Basing calcs on:");
	System.out.println("Defectors: " + defN);
	System.out.println("Cooperators: " + coopN);
	System.out.println("Inspectors: " + inspN);
	// Based on the expected values for the next round, calculate the best
	// round.
	// Extract expected value of choosing to play as cooperator
	double moneyC = (x * coopN * r) / (defN + coopN) - x;
	// Extract expected value of choosing to play as defector
	double moneyD = (x * coopN * r) / (defN + coopN);
	// Extract expected value of being inspected when defecting
	double moneyDInspected = moneyD * (-1.00d) * (m);

	double moneyI = moneyD * (1.00d + m);
	// Now we are going to build some procs:
	/**
	 * Not being inspected means that every inspector choose other player
	 * (1-P(choosing me))*(Number of Inspectors)
	 */
	double pChooseMe = Math.pow((defN + coopN), -1);
	System.out.println("CHOOSING  1 PLAYER RANDOMLY: " + pChooseMe);
	System.out.println("CHOOSE ANY OTHER BUT ME: " + (1 - pChooseMe));
	double pNotBeingInspected = Math.pow((1 - pChooseMe), inspN);
	/**
	 * Proc of inspecting a defector, if we choose randomly.
	 */
	double inspectADefector = defN / (defN + coopN);
	System.out.println("Money for cooperator: " + moneyC);
	System.out.println("Money for defector not inspected: " + moneyD);
	System.out.println("Money for defector inspected: " + moneyDInspected);
	System.out.println("Proc of being inspected: " + (1.00d - pNotBeingInspected));
	double expectedMforD = ((1 - pNotBeingInspected) * moneyDInspected) + (moneyD * pNotBeingInspected);
	System.out.println("Expected benefit for D: " + expectedMforD);

	// Now perform same calcs for inspector
	/**
	 * Proc of being the only inspector=1-(Not being the only one) Proc of
	 * not being the only one=P(being 2)+P(being 3)+(...)
	 */
	double accumulation = 0.0d;
	/**
	 * Contains the proc of a player being inspected bye exactly N
	 * inspectors
	 */
	double[] procOfBeingNinspectors = new double[NiMax];
	for (int i = 2; i < NiMax; i++) {
	    procOfBeingNinspectors[i] = Math.pow(1.00d / (defN + coopN), i);
	    accumulation += procOfBeingNinspectors[i] * (moneyI / i);
	    System.out.println("Proc of being: " + i + " inspectors is: " + procOfBeingNinspectors[i]);
	    System.out.println("In that case, each one gets: " + (moneyI / i));

	}
	// Expected for inspector is now:
	double procOfNotBeingUniqueInspector = 0.0d;
	for (int i = 2; i < NiMax; i++) {
	    procOfNotBeingUniqueInspector += procOfBeingNinspectors[i];
	}
	System.out.println("PRoc of being unique inspector is: " + (1 - procOfNotBeingUniqueInspector));
	double expectedMI = ((1 - procOfNotBeingUniqueInspector) * moneyI) + accumulation;
	System.out.println("Expected benefit for inspector is: " + expectedMI);

	if (expectedMforD > expectedMI && expectedMforD > moneyC)
	    bestRole = Psi7Player.DEFECTOR;
	if (expectedMI > expectedMforD && expectedMI > moneyC)
	    bestRole = Psi7Player.INSPECTOR;
	if (moneyC > expectedMI && moneyC > expectedMforD)
	    bestRole = Psi7Player.COOPERATOR;

	return bestRole;
    }

}
