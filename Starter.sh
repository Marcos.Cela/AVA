#Start jade environment with various agents
LINE=645
#Number of fixed Inspectors
FIXED_I=1
#Number of fixed Cooperators
FIXED_C=1
#Number of fixed Defectors
FIXED_D=0
#Number of Random players
RAND=0
#Log level for agents
LOG_LEVEL="OFF"
#Names for agentes, and prefixes
MAIN_AGENT_NAME="MainAg"
FIXED_I_PREFIX="FIXED_I"
FIXED_C_PREFIX="FIXED_C"
FIXED_D_PREFIX="FIXED_D"
RANDOM_PREFIX="RANDOM"

#Separator between PREFIX and AGENT_N
SEPARATOR="_"
#Initialize variable with, at least, Main Agent
AGENTS="$MAIN_AGENT_NAME:psi7_MainAg;"
#Initialize each kind of player with the given log level

LOG_LEVEL_PLAYERS="OFF"
#Initialize random players
for (( c=0; c<$RAND; c++ ))
do
	AGENTS+="$RANDOM_PREFIX$SEPARATOR$c:psi7_Random($LOG_LEVEL_PLAYERS);"
done

#Initialize cooperators players
for (( c=0; c<$FIXED_C; c++ ))
do
	AGENTS+="$FIXED_C_PREFIX$SEPARATOR$c:psi7_FixedC($LOG_LEVEL_PLAYERS);"
done

#Initialize inspectors players
for (( c=0; c<$FIXED_I; c++ ))
do
	AGENTS+="$FIXED_I_PREFIX$SEPARATOR$c:psi7_FixedI($LOG_LEVEL_PLAYERS);"
done

#Initialize inspectors players
for (( c=0; c<$FIXED_D; c++ ))
do
	AGENTS+="$FIXED_D_PREFIX$SEPARATOR$c:psi7_FixedD($LOG_LEVEL_PLAYERS);"
done

echo $AGENTS
java -cp 'jars/*:bin' jade.Boot -agents $AGENTS



