-----------------------------------------------------------------------------------------------------
-Name: 					Marcos Cela López															-
-Lab-Account: 			psi7																		-
-----------------------------------------------------------------------------------------------------
*****************************************************************************************************
*							       COMPILING THE PROGRAM											*
*****************************************************************************************************
With a terminal, navigate to the directory where this readme.txt file is, you should see
several folders (bin,src,jars...).
	
Run the following command:
		javac -cp 'jars/*' -d bin src/*.java
	
This will compile all the .java files located at the "src" folder, linked with the external .jars
located at the "jars" folder and output the generated .class files at the "bin" folder.

Alternatively you can also use the script "compiler.sh" (Remember to give it the correct permissions),
using CHMOD.

Notes:
	- Remember that if you want your class files to be in the 'bin' folder, you must create it,
	javac will not create it for you.
*****************************************************************************************************
*							        	RUNNING THE PROGRAM											*
*****************************************************************************************************
Once we have compiled all the files, with a terminal navigate to the directory where this readme.txt
file is, you should see the bin,src and jars folders here.
	
Run the following command, setting as much agents as you want (remember to follow the jade pattern,
 and don't give agents duplicated names):
	java -cp 'jars/*:bin' jade.Boot -agents 'MA:psi7_MainAg;FixC:psi7_FixedC;Rand:psi7_Random;'
	
Notes:
	- The inclusion of the agent psi7_MainAgent is mandatory. You can add  more players by adding
	the player name and class (Must be located at the bin folder) to the command.

	- If you want to run external agents (agentes developed by other students):
		1.- Ensure that all the needed .class files are in the bin folder.
		2.- Ensure that if the player uses external tools they are at the correct folder (jars).
*****************************************************************************************************
*							RUNNING THE PROGRAM WITH EXTERNAL AGENTS								*
*****************************************************************************************************
If you want to use the main agent provided, but want to test it with agents developed by other
students, you must carefully follow the next steps.

1. Copy all the needed *.class files needed by the agents (They might use inheritance, so you may
have to copy several files) and place them on the 'bin' folder of this project.

2. If the agents use any external tool packed in a .jar file (such as Log4j for example) you must
copy the needed jar files to the jars folder on this project.
	
Example:
If we want to test this application with the random agent provided by psi11, we must copy the file
named 'psi11_Random.class' (Assuming it does not have any other dependencies) to our 'bin' folder.

Then we just use the following command:
	java -cp 'jars/*:bin' jade.Boot -agents 'MA:psi7_MainAg;FixC:psi7_FixedC;RAND_EXT:psi11_Random;'

*****************************************************************************************************
*							RUNNING PSI7 AGENTS WITH EXTERNAL MAIN AGENT							*
*****************************************************************************************************
If you want to use psi7_xxx agents with the main agent developed by other student, you just have
to compile it, and remember that the inclussion of psi7_Player is mandatory, since ALL psi7_xxx
players will inherit from it.

PSI7 agents do NOT use any external .jar file, so there is no need to include those.
*****************************************************************************************************
*										ADDITIONAL FEATURES											*
*****************************************************************************************************
You can set different verbose levels, this provides you with more control about the information
that is being displayed (You can only show critical errors, or you can choose to show very detailed
information, such as message contents and so on).
Be careful, because setting all verbose levels (Main agent and players) to very detailed values
might slow the system. The recommended value is INFO.

The general game information will appear in a table, and you can see per-round information on the
"Round Information" tab. Verbose output will go both to the system console and to the "Verbose"
tab.

If you click on any player while the game is paused, you can choose to view extended information
with the help of charts, thus making data interpretation much faster.

Note: Win ratio refers to the % of the rounds that a player has completed with a benefit
strictly superior to zero (>0).




